package com.meingustrow.app.model;


import static com.meingustrow.app.model.FileModel.MediaType.MEDIA_TYPE_IMAGE;

/**
 * Created by viraj.patel on 24-Apr-19
 */
public class FileModel {

    public interface MediaType{
        String MEDIA_TYPE_IMAGE = "image";
        String MEDIA_TYPE_VIDEO = "video";
    }

    private String name = "", key = "", path = "", type = MEDIA_TYPE_IMAGE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "FileModel{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", path='" + path + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
