package com.meingustrow.app.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class TypeConvertersCategory {
    Gson gson = new Gson();

    @TypeConverter
    public static List<MainCategoryModel> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<MainCategoryModel>>() {
        }.getType();

        return new Gson().fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<MainCategoryModel> someObjects) {
        return new Gson().toJson(someObjects);
    }
}
