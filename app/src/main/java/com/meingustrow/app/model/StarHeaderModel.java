package com.meingustrow.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class StarHeaderModel {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Integer id;
    @Expose
    @SerializedName("Gutscheinguthaben")
    private String Gutscheinguthaben;
    @Expose
    @SerializedName("Punkte")
    private String Punkte;
    @Expose
    @SerializedName("Guthaben")
    private String Guthaben;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGutscheinguthaben() {
        return Gutscheinguthaben;
    }

    public void setGutscheinguthaben(String Gutscheinguthaben) {
        this.Gutscheinguthaben = Gutscheinguthaben;
    }

    public String getPunkte() {
        return Punkte;
    }

    public void setPunkte(String Punkte) {
        this.Punkte = Punkte;
    }

    public String getGuthaben() {
        return Guthaben;
    }

    public void setGuthaben(String Guthaben) {
        this.Guthaben = Guthaben;
    }
}
