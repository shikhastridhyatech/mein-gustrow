package com.meingustrow.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "MainCategory")
public class MainCategoryModel {

     /*   @PrimaryKey(autoGenerate = true)
        private int idMain;*/

    @PrimaryKey
    @ColumnInfo(name = "category_id")
    @SerializedName("id")
    @Expose
    private int categoryId;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    @Expose
    private String title;

    @ColumnInfo(name = "items")
    @SerializedName("items")
    @Expose
    private String items;
    @Ignore
    List<SubContentModel> arraySubContnet;

    @Ignore
    List<HomeModel> arrayHome;

    public List<HomeModel> getArrayHome() {
        return arrayHome;
    }

    public void setArrayHome(List<HomeModel> arrayHome) {
        this.arrayHome = arrayHome;
    }

    public List<SubContentModel> getArraySubContnet() {
        return arraySubContnet;
    }

    public void setArraySubContnet(List<SubContentModel> arraySubContnet) {
        this.arraySubContnet = arraySubContnet;
    }

    /*public int getIdMain() {
            return idMain;
        }

        public void setIdMain(int idMain) {
            this.idMain = idMain;
        }
    */
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

}
