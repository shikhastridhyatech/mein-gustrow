package com.meingustrow.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Weather")
public class WeatherModel {
   /* @PrimaryKey(autoGenerate = true)
    private int id;*/

    @PrimaryKey
    @ColumnInfo(name = "id")
    private int uniqueId;

    @Expose
    @SerializedName("weekday")
    private String weekday;
    @Expose
    @SerializedName("date")
    private String date;

    @Ignore
    @Expose
    @SerializedName("statusCode")
    private int statusCode;

    @Ignore
    @Expose
    @SerializedName("error")
    private String error;

    @Embedded
    @Expose
    @SerializedName("data")
    private WeatherDataModel data;

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

  /*  public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public WeatherDataModel getData() {
        return data;
    }

    public void setData(WeatherDataModel data) {
        this.data = data;
    }

}
