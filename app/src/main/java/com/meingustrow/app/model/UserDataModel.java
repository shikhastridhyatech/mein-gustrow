package com.meingustrow.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by viraj.patel on 08-04-2019.
 */
public class UserDataModel implements Parcelable {


    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("isIMEIVerified")
    private boolean isIMEIVerified;
    @SerializedName("userId")
    private Integer userId;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("token")
    private String token;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isIMEIVerified() {
        return isIMEIVerified;
    }

    public void setIMEIVerified(boolean IMEIVerified) {
        isIMEIVerified = IMEIVerified;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "isActive=" + isActive +
                ", isIMEIVerified=" + isIMEIVerified +
                ", userId=" + userId +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isIMEIVerified ? (byte) 1 : (byte) 0);
        dest.writeValue(this.userId);
        dest.writeString(this.email);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.imageUrl);
        dest.writeString(this.token);
    }

    public UserDataModel() {
    }

    protected UserDataModel(Parcel in) {
        this.isActive = in.readByte() != 0;
        this.isIMEIVerified = in.readByte() != 0;
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.email = in.readString();
        this.phoneNumber = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.imageUrl = in.readString();
        this.token = in.readString();
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel source) {
            return new UserDataModel(source);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };
}

