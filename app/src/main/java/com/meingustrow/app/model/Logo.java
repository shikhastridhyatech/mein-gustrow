package com.meingustrow.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Logo {
    public Logo(String light, String dark) {
        this.light = light;
        this.dark = dark;
    }

    @ColumnInfo(name = "logo_light")
    @SerializedName("light")
    @Expose
    private String light;
    @ColumnInfo(name = "logo_dark")
    @SerializedName("dark")
    @Expose
    private String dark;

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getDark() {
        return dark;
    }

    public void setDark(String dark) {
        this.dark = dark;
    }

}
