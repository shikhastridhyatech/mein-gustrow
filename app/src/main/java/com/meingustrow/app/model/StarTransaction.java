package com.meingustrow.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "StarTransaction", indices = @Index(value = {"Zeitpunkt"}, unique = true) )
public class StarTransaction {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Integer id;
    private String punkte;
    private String Guthaben;
    private String Gutscheinguthaben;
    @Expose
    @SerializedName("Zeitpunkt")
    private String Zeitpunkt;//"Zeitpunkt":"2021-03-12 07:50:39" YYYY-MM-dd HH:mm:ss
    @Expose
    @SerializedName("BonusPunkte")
    private int BonusPunkte;
    @Expose
    @SerializedName("BonusCents")
    private int BonusCents;
    @Expose
    @SerializedName("Betrag")
    private int Betrag;
    @Expose
    @SerializedName("BonusEuro")
    private String BonusEuro;
    @Expose
    @SerializedName("Akzeptanzstelle")
    private String Akzeptanzstelle;

    public String getZeitpunkt() {
        return Zeitpunkt;
    }

    public void setZeitpunkt(String Zeitpunkt) {
        this.Zeitpunkt = Zeitpunkt;
    }

    public int getBonusPunkte() {
        return BonusPunkte;
    }

    public void setBonusPunkte(int BonusPunkte) {
        this.BonusPunkte = BonusPunkte;
    }

    public int getBonusCents() {
        return BonusCents;
    }

    public void setBonusCents(int BonusCents) {
        this.BonusCents = BonusCents;
    }

    public int getBetrag() {
        return Betrag;
    }

    public void setBetrag(int Betrag) {
        this.Betrag = Betrag;
    }

    public String getAkzeptanzstelle() {
        return Akzeptanzstelle;
    }

    public String getPunkte() {
        return punkte;
    }

    public void setPunkte(String punkte) {
        this.punkte = punkte;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setAkzeptanzstelle(String Akzeptanzstelle) {
        this.Akzeptanzstelle = Akzeptanzstelle;
    }

    public String getBonusEuro() {
        return BonusEuro;
    }

    public void setBonusEuro(String bonusEuro) {
        BonusEuro = bonusEuro;
    }

    public String getGuthaben() {
        return Guthaben;
    }

    public void setGuthaben(String guthaben) {
        Guthaben = guthaben;
    }

    public String getGutscheinguthaben() {
        return Gutscheinguthaben;
    }

    public void setGutscheinguthaben(String gutscheinguthaben) {
        Gutscheinguthaben = gutscheinguthaben;
    }
}