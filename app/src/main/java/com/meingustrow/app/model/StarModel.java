package com.meingustrow.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StarModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private int statusCode;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public class Data {
        @Expose
        @SerializedName("Transaktionen")
        private List<StarTransaction> Transaktionen;

        public List<StarTransaction> getTransaktionen() {
            return Transaktionen;
        }

        public void setTransaktionen(List<StarTransaction> Transaktionen) {
            this.Transaktionen = Transaktionen;
        }
    }
}
