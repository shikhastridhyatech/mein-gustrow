package com.meingustrow.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "SubContent")
public class SubContentModel {

    /*@PrimaryKey(autoGenerate = true)
    private int subcontent_id;*/

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    private Integer id;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    @Expose
    private String title;

    @ColumnInfo(name = "image")
    @SerializedName("image")
    @Expose
    private String image;

    @Embedded
    @SerializedName("logo")
    @Expose
    private Logo logo;

    @Embedded
    @SerializedName("icon")
    @Expose
    private Icon icon;

    @ColumnInfo(name = "sub_content")
    @SerializedName("sub_content")
    @Expose
    private String subContent;

    @ColumnInfo(name = "url")
    @SerializedName("url")
    @Expose
    private String url;

    /*public int getSubcontent_id() {
        return subcontent_id;
    }

    public void setSubcontent_id(int subcontent_id) {
        this.subcontent_id = subcontent_id;
    }
*/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }

    /*@Entity
    public class Icon {
        @ColumnInfo(name = "icon_light")
        @SerializedName("light")
        @Expose
        private String light;

        @ColumnInfo(name = "icon_dark")
        @SerializedName("dark")
        @Expose
        private String dark;

        public String getLight() {
            return light;
        }

        public void setLight(String light) {
            this.light = light;
        }

        public String getDark() {
            return dark;
        }

        public void setDark(String dark) {
            this.dark = dark;
        }

    }

    @Entity
    public class Logo {
        @ColumnInfo(name = "logo_light")
        @SerializedName("light")
        @Expose
        private String light;
        @ColumnInfo(name = "logo_dark")
        @SerializedName("dark")
        @Expose
        private String dark;

        public String getLight() {
            return light;
        }

        public void setLight(String light) {
            this.light = light;
        }

        public String getDark() {
            return dark;
        }

        public void setDark(String dark) {
            this.dark = dark;
        }

    }*/
}
