package com.meingustrow.app.ui.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.zxing.WriterException;
import com.meingustrow.app.R;
import com.meingustrow.app.databinding.FragmentQrCodeBinding;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.Utils;

import java.util.Objects;

public class QRCodeFragment extends BaseFragment {

    private static final String TAG = QRCodeFragment.class.getName();
    private FragmentQrCodeBinding mBinding;

    public QRCodeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_qr_code, container, false);


        float width = getResources().getDimension(R.dimen._140sdp);
        float height = getResources().getDimension(R.dimen._140sdp);
        Log.e(TAG, "width........" + width);
        Log.e(TAG, "height........" + height);
        try {
//            Bitmap bitmap = Utils.convertQRCode("6036003021000152", (int) width, (int) height);
            Bitmap bitmap = Utils.convertQRCode(Utils.getGustrowData(me).getQRCode());

            Glide.with(me)
                    .load(bitmap)
                    .into(mBinding.ivQrCode);

            mBinding.tvNumber.setText(Utils.getGustrowData(me).getQRCode());
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return mBinding.getRoot();
    }

    /*@Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Objects.requireNonNull(((DashboardActivity) me)).getSupportFragmentManager().popBackStack();
            Utils.replaceFragmentDashboard((AppCompatActivity) me, new LoginFragment(""), true);
            Log.e(TAG, "result......................................................." + result);
        }
    }*/

    @Override
    public void refresh(String dialogString) {
        Log.e(TAG, "refresh.................");
       /* new Handler().post(new Runnable() {
            public void run() {
                Utils.replaceFragmentDashboard((AppCompatActivity) me, new LoginFragment(""), true);
            }
        });*/

//        getChildFragmentManager().beginTransaction().replace(R.id.frameDashboardMain, new LoginFragment(""), TAG).addToBackStack(TAG).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            BottomMenuFragment.mBinding.bottomNavigation.getMenu().getItem(0).setChecked(true);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.imgNavigationIcon.setVisibility(View.VISIBLE);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.tvLogout.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("QRCode", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("QRCodeScreen", bundle);
    }
}
