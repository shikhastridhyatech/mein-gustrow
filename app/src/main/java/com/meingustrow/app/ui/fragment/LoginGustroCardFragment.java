package com.meingustrow.app.ui.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.meingustrow.app.R;
import com.meingustrow.app.Retrofit.ApiRetrofit;
import com.meingustrow.app.broadcast.MyAlarm;
import com.meingustrow.app.databinding.FragmentLoginNewBinding;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.model.GustrowLoginModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.AlertUtils;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginGustroCardFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = LoginGustroCardFragment.class.getName();
    private FragmentLoginNewBinding mBinding;
    private String mString = "", mStrDate = "";
    private DatabaseMein mDatabaseMein;
    private Calendar mCal;

    public LoginGustroCardFragment() {
    }

    public LoginGustroCardFragment(String string) {
        this.mString = string;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_new, container, false);

//        mBinding.edtQrCode.setText("6036003021000152");
//        mBinding.edtPostalCode.setText("12345");

        try {
            mDatabaseMein = DatabaseMein.getInstance(me);

            mCal = Calendar.getInstance();

            mBinding.tvLogin.setOnClickListener(this);
            mBinding.edtDOB.setOnClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return mBinding.getRoot();
    }

    private void openCalendar() {
        Locale locale = new Locale("DE");
        Locale.setDefault(locale);

//        Locale locale = getResources().getConfiguration().locale;
//        Locale.setDefault(locale);

        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(me, R.style.MySpinnerDatePickerStyle, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datepicker, int year, int monthOfYear, int dayOfMonth) {
                mCal.set(year, monthOfYear, dayOfMonth);
                String month;
                String date = "" + dayOfMonth;
                if ((monthOfYear + 1) < 10) {
                    month = "0" + (monthOfYear + 1);
                } else {
                    month = "" + (monthOfYear + 1);
                }
                if (dayOfMonth < 10) {
                    date = "0" + dayOfMonth;
                }
                String DOB = date + "." + month + "." + year;
                mStrDate = year + "-" + month + "-" + date;
                mBinding.edtDOB.setText(DOB);
            }
        }, mYear, mMonth, mDay);
        //mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

   /* private void setDatePicker() {
        try {
            DatePickerDialog dpd = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                mCal.set(year, monthOfYear, dayOfMonth);
                String month;
                String date = "" + dayOfMonth;
                if ((monthOfYear + 1) < 10) {
                    month = "0" + (monthOfYear + 1);
                } else {
                    month = "" + (monthOfYear + 1);
                }
                if (dayOfMonth < 10) {
                    date = "0" + dayOfMonth;
                }
                String DOB = date + "." + month + "." + year;
                mStrDate = year + "-" + month + "-" + date;
                mBinding.edtDOB.setText(DOB);

            }, mCal.get(Calendar.YEAR), mCal.get(Calendar.MONTH), mCal.get(Calendar.DAY_OF_MONTH));
            dpd.setLocale(new Locale("DE"));
            dpd.show(Objects.requireNonNull(getFragmentManager()), "Datepickerdialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /*@Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Log.e(TAG, "result......................................................." + result);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((DashboardActivity) Objects.requireNonNull(mContext)).mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
            ((DashboardActivity) Objects.requireNonNull(mContext)).mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);

            if (mString.equalsIgnoreCase(getString(R.string.from_settings))) {
                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.login);
            } else {
                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.gustrow_card);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("GustrowCardLogin", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("GustrowCardLoginScreen", bundle);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvLogin:
                View parentLayout = getActivity().findViewById(android.R.id.content);
                try {
                    if (Objects.requireNonNull(mBinding.edtQrCode.getText()).toString().trim().equals("")) {
                        showSnackBar(parentLayout, getString(R.string.blank_card_number));
                    } else if (mBinding.edtPostalCode.getText().toString().trim().equals("")) {
                        showSnackBar(parentLayout, getString(R.string.blank_postal_code));
                    } else if (mBinding.edtPostalCode.getText().toString().trim().length() != 5) {
                        showSnackBar(parentLayout, getString(R.string.postal_code_not_valid));
                    } else if (mBinding.edtDOB.getText().toString().trim().equals("")) {
                        showSnackBar(parentLayout, getString(R.string.blank_birth_date));
                    } else {
                        if (Utils.isConnectingToInternet(me)) {
                            signinApi();
                        } else {
                            Utils.noInternet(me);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.edtDOB:
//                setDatePicker();
                openCalendar();

                break;
        }
    }

    private void signinApi() {
        try {
            Dialog dialog = showProgressBar();
            dialog.show();
            Log.e(TAG, "mStrDate........." + mStrDate);
            Log.e(TAG, "edtQrCode........." + mBinding.edtQrCode.getText());
            Log.e(TAG, "edtPostalCode........." + mBinding.edtPostalCode.getText());
            Call<ResponseBody> call = ApiRetrofit.getClient().
                    signInGustroUser(Objects.requireNonNull(mBinding.edtQrCode.getText()).toString().trim(),
                            mBinding.edtPostalCode.getText().toString().trim(), mStrDate);
            /*Call<ResponseBody> call = ApiRetrofit.getClient().
                    signInGustroUser("6036003021000152",
                            "12345", "1990-01-01");*/
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        hideProgressBar();

                        try {
                            assert response.body() != null;
                            String responseString = response.body().string();
                            Log.e(TAG, "signin---Gustrow-----------" + responseString);
                            JSONObject jsonObject = new JSONObject(responseString);

                            if (jsonObject.has("data")) {
                               /* if (jsonObject.optString("data").equals("null")) {

                                }*/
                                if (jsonObject.isNull("data")) {
                                    Log.e(TAG, "null-------------------");
                                }
                            }
                            if (jsonObject.optInt("statusCode") == 404 || jsonObject.optInt("statusCode") == 400) {
                                AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), jsonObject.optString("message"), true, "", getString(R.string.ok), new OnConfirmationDialog() {
                                    @Override
                                    public void onYes() {
                                    }

                                    @Override
                                    public void onNo() {
                                    }
                                });
                            } else {
                               /* if (jsonObject.isNull("data")) {
                                    AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), jsonObject.optString("message"), true, "", getString(R.string.ok), new OnConfirmationDialog() {
                                        @Override
                                        public void onYes() {
                                        }

                                        @Override
                                        public void onNo() {
                                        }
                                    });
                                } else {*/
                                if (!mBinding.cbLogin.isChecked()) {
                                    new MyAlarm(me, 1800, LoginGustroCardFragment.class.getName(), AppConstants.REQUEST_CODE_GUSTROW);
                                }

                                Utils.setLogInGustrow(me, true);

                                GustrowLoginModel gustrowLoginModel = new GustrowLoginModel();
                                gustrowLoginModel.setBirthDate(mStrDate);
                                gustrowLoginModel.setQRCode(mBinding.edtQrCode.getText().toString());
                                gustrowLoginModel.setPostalCode(mBinding.edtPostalCode.getText().toString());

                                Utils.setGustrowData(me, gustrowLoginModel);
                                ApiDatabase.starTransactionApi(Utils.getGustrowData(me), null, mDatabaseMein, TAG, (AppCompatActivity) me, null);
                                Utils.replaceFragmentDashboard((AppCompatActivity) me, new BottomMenuFragment(), true, true);
//                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    Log.e(TAG, "call...." + call.toString());
                    hideProgressBar();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
