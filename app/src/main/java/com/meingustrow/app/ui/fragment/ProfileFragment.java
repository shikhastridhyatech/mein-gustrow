package com.meingustrow.app.ui.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.meingustrow.app.R;
import com.meingustrow.app.broadcast.MyAlarm;
import com.meingustrow.app.databinding.FragmentProfileBinding;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.AlertUtils;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;
import com.meingustrow.app.webservice.APIs;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = ProfileFragment.class.getName();
    private FragmentProfileBinding mBinding;

    public ProfileFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        try {
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.tvLogout.setOnClickListener(this);

//        if (Utils.isConnectingToInternet(me)) {
//            Utils.showProgressBar(me);

            mBinding.webView.getSettings().setAllowContentAccess(true);
            mBinding.webView.getSettings().setAllowFileAccess(true);
            mBinding.webView.getSettings().setAllowFileAccessFromFileURLs(true);
            mBinding.webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            mBinding.webView.getSettings().setJavaScriptEnabled(true);
            mBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mBinding.webView.setClickable(true);
            mBinding.webView.setWebChromeClient(new WebChromeClient());
            mBinding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

            mBinding.webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    //                    Utils.dismissProgressBar();
                }
            });

            mBinding.webView.loadUrl(APIs.TEMP_WEB);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mBinding.getRoot();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Log.e(TAG, "result......................................................." + result);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            BottomMenuFragment.mBinding.bottomNavigation.getMenu().getItem(4).setChecked(true);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.imgNavigationIcon.setVisibility(View.GONE);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.tvLogout.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvLogout:
                try {
                    AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), getString(R.string.want_to_log_out), false, getString(R.string.no), getString(R.string.yes), new OnConfirmationDialog() {
                        @Override
                        public void onYes() {

                            try {
                                Intent intent = new Intent(me, MyAlarm.class);
                                boolean alarmUp = (PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_GUSTROW,
                                        intent,
                                        PendingIntent.FLAG_NO_CREATE) != null);

                                Log.e(TAG, "alarmUp............" + alarmUp);
                                if (alarmUp) {
                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_GUSTROW, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    AlarmManager alarmManager = (AlarmManager) me.getSystemService(Context.ALARM_SERVICE);
                                    pendingIntent.cancel();
                                    assert alarmManager != null;
                                    alarmManager.cancel(pendingIntent);
                                }

                                Utils.setLogInGustrow(me, false);
                                Utils.setAlarmGustrow(me, false);
                                Utils.clearGustrowData(me);

                                Intent intent1 = new Intent("refresh");
                                intent1.putExtra(AppConstants.ALARM_MANAGER_DIALOG, "");
                                LocalBroadcastManager.getInstance(me).sendBroadcast(intent1);
                                startActivity(new Intent(me, DashboardActivity.class));
                                me.finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNo() {
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
