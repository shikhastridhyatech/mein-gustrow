package com.meingustrow.app.ui.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.meingustrow.app.R;
import com.meingustrow.app.databinding.FragmentWebBinding;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class WebFragment extends BaseFragment {

    private static final String TAG = WebFragment.class.getName();
    public static FragmentWebBinding mBinding;
    private String mUrl = "", mTitle = "", mTitleMain = "";

    public WebFragment() {
    }

    public WebFragment(String url, String title) {
        this.mUrl = url;
        this.mTitle = title;
    }

    public WebFragment(String titleMain, String url, String title) {
        this.mUrl = url;
        this.mTitle = title;
        this.mTitleMain = titleMain;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_web, container, false);

//        if (Utils.isConnectingToInternet(me)) {
//            Utils.showProgressBar(me);

        setWebView();

        return mBinding.getRoot();
    }

    private void setWebView() {
        try {
            mBinding.webView.getSettings().setAllowContentAccess(true);
            mBinding.webView.getSettings().setAllowFileAccess(true);
            mBinding.webView.getSettings().setAllowFileAccessFromFileURLs(true);
            mBinding.webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            mBinding.webView.getSettings().setJavaScriptEnabled(true);
            mBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mBinding.webView.setClickable(true);
            mBinding.webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            mBinding.webView.setWebChromeClient(new WebChromeClient());
            mBinding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            mBinding.webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

            mBinding.webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    Log.e(TAG, "onPageStarted: ");
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    Log.e(TAG, "onPageFinished: ");
                    //                    Utils.dismissProgressBar();
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.e(TAG, "shouldOverrideUrlLoading: ");
                    if (url.endsWith(".pdf")) {
                        String googleDocs = "https://docs.google.com/gview?embedded=true&url=";
                        mBinding.webView.loadUrl(googleDocs + url);
                        // Load "url" in google docs

//                        https://docs.google.com/gview?embedded=true&url=
                    } else {
                        // Load all other urls normally.
                        view.loadUrl(url);
                    }

                    return super.shouldOverrideUrlLoading(view, url);
                }

            });

            Log.e(TAG, "mUrl..............." + mUrl);
            mBinding.webView.loadUrl(mUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Log.e(TAG, "result......................................................." + result);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);
            if (mTitleMain.equalsIgnoreCase("")) {
                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(mTitle);
            } else {
//                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(mTitleMain + " - " + mTitle);
                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(mTitle);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("WebView", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("WebViewScreen", bundle);
    }
}
