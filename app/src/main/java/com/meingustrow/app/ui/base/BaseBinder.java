package com.meingustrow.app.ui.base;

import android.content.res.Resources;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.meingustrow.app.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.meingustrow.app.webservice.APIs.BASE_IMAGE_PATH;

public class BaseBinder {

    @BindingAdapter({"app:setLanguageIcon"})
    public static void setFlagIcon(AppCompatImageView iv, String image) {

        if (image != null && image.length() > 0 && !image.equals("null")) {
            Resources resources = iv.getContext().getResources();
            final int resourceId = resources.getIdentifier(image, "drawable",
                    iv.getContext().getPackageName());
            iv.setImageResource(resourceId);
        } else {
            iv.setImageResource(R.mipmap.ic_launcher);
        }
    }

    @BindingAdapter({"app:setSimpleImage"})
    public static void setSimpleImage(AppCompatImageView iv, String image) {

        if (image != null && image.length() > 0 && !image.equals("null"))
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + image)
                    .apply(new RequestOptions()
                            .placeholder(R.color.transparent)
                            .error(R.color.grey)
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .centerCrop())
                    .transition(withCrossFade(300))
                    .into(iv);
        else
            iv.setImageResource(R.mipmap.ic_launcher);
    }
}
