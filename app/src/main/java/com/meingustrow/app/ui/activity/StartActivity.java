package com.meingustrow.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ActivitySplashAnimationBinding;
import com.meingustrow.app.interfaces.OnCompleteApi;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.base.BaseActivity;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class StartActivity extends BaseActivity {
    private static final String TAG = StartActivity.class.getName();
    private ActivitySplashAnimationBinding mBinding;
    private final int FIVE_SECONDS = 5000;
    private Handler handler = new Handler();
    private List<MainCategoryModel> mArrayMainCategory = new ArrayList<>();
    private List<SubContentModel> mArrayContent = new ArrayList<>();
    private DatabaseMein mDatabaseMein;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.setContentView(me, R.layout.activity_splash_animation);

        mDatabaseMein = DatabaseMein.getInstance(me);

        Log.e(TAG, "mDatabaseMein size..................." + mDatabaseMein.daoAccess().getAllContent().size());
        if (mDatabaseMein.daoAccess().getAllContent().size() != 0) {
            setAnimation("");
        } else {
            if (Utils.isConnectingToInternet(me)) {
                setAnimation("firstTime");
            } else {
                try {
                    ApiDatabase.showCustomConfirmationDialog(me, true, getString(R.string.keine_internetverbindung), getString(R.string.sub_splash), true, "", getString(R.string.widerholen), new OnConfirmationDialog() {
                        @Override
                        public void onYes() {
                            setAnimation("firstTime");
                            scheduleEvery5Sec();
                        }

                        @Override
                        public void onNo() {
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setAnimation(String string) {
        try {
            new CountDownTimer(500, 500) {
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {

                    try {
                        if (Utils.getTheme(me) == R.style.ThemeLight) {
                            Glide.with(me)
                                    .load(R.drawable.splash_light)
                                    .into(mBinding.imgLogo);
                        } else {
                            Glide.with(me)
                                    .load(R.drawable.splash_dark)
                                    .into(mBinding.imgLogo);
                        }
                        if (Utils.isConnectingToInternet(me)) {
                            ApiDatabase.weatherApi(session, mDatabaseMein, TAG, me, null);
                            ApiDatabase.categoryApi(session, null, mDatabaseMein, string, TAG, me, new OnCompleteApi() {
                                @Override
                                public void onComplete() {
                                    if (string.equalsIgnoreCase("firstTime")) {
                                        new CountDownTimer(500, 500) {
                                            public void onTick(long millisUntilFinished) {
                                            }

                                            @Override
                                            public void onFinish() {

                                                Intent intent = new Intent(me, DashboardActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }.start();
                                    }
                                }
                            });
                            if (string.equalsIgnoreCase("")) {
                                new CountDownTimer(500, 500) {
                                    public void onTick(long millisUntilFinished) {
                                    }

                                    @Override
                                    public void onFinish() {

                                        Intent intent = new Intent(me, DashboardActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }.start();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    private void scheduleEvery5Sec() {
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    ApiDatabase.showCustomConfirmationDialog(me, true, getString(R.string.keine_internetverbindung), getString(R.string.sub_splash), true, "", getString(R.string.widerholen), new OnConfirmationDialog() {
                        @Override
                        public void onYes() {
                            setAnimation("firstTime");
                        }

                        @Override
                        public void onNo() {
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                handler.postDelayed(this, FIVE_SECONDS);
            }
        }, FIVE_SECONDS);
    }
}
