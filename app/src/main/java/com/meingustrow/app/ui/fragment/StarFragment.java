package com.meingustrow.app.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.meingustrow.app.R;
import com.meingustrow.app.adapter.StarAdapter;
import com.meingustrow.app.databinding.FragmentStarBinding;
import com.meingustrow.app.interfaces.OnCompleteApi;
import com.meingustrow.app.model.StarHeaderModel;
import com.meingustrow.app.model.StarTransaction;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StarFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = StarFragment.class.getName();
    private FragmentStarBinding mBinding;
    private StarAdapter mStarAdapter;
    private DatabaseMein mDatabaseMein;
    private List<StarTransaction> mArrayTransaction = new ArrayList<>();
    private StarHeaderModel mStarHeaderModel;

    public StarFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_star, container, false);

        try {
            mDatabaseMein = DatabaseMein.getInstance(me);
            mBinding.swipeRefresh.setOnRefreshListener(this::onRefresh);

            onRefresh();

            setFromDB();
            setAdapter();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBinding.getRoot();
    }

    private void setFromDB() {
        try {
            mArrayTransaction = mDatabaseMein.daoAccess().getAllStarTransaction();
//        mStarHeaderModel = mDatabaseMein.daoAccess().getAllStarHeaders();
            if (mArrayTransaction != null && mArrayTransaction.size() > 0) {
//                mBinding.tvCount.setText(mArrayTransaction.get(0).getPunkte());
                // mBinding.tvGuthaben.setText(mArrayTransaction.get(0).getGuthaben() + getContext().getString(R.string.EURO));
                mBinding.tvGuthabenEuro.setText(mArrayTransaction.get(0).getGuthaben() + " "/* + getContext().getString(R.string.EURO)*/);
                //  mBinding.tvGuthabenEuro.setText("9999,99" + " "/* + getContext().getString(R.string.EURO)*/);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Log.e(TAG, "result......................................................." + result);
        }
    }*/

    private void setAdapter() {
        try {
            mStarAdapter = new StarAdapter(me, mArrayTransaction);
            LinearLayoutManager layoutManager = new LinearLayoutManager(me, RecyclerView.VERTICAL, false);
            mBinding.rvStar.setLayoutManager(layoutManager);
            mBinding.rvStar.setAdapter(mStarAdapter);
            mBinding.rvStar.setNestedScrollingEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            BottomMenuFragment.mBinding.bottomNavigation.getMenu().getItem(1).setChecked(true);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.imgNavigationIcon.setVisibility(View.VISIBLE);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.tvLogout.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("Star", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("StarScreen", bundle);
    }

    @Override
    public void onRefresh() {
        try {
            if (Utils.isConnectingToInternet(me)) {
//                mDatabaseMein.daoAccess().deleteTransaction();
                ApiDatabase.starTransactionApi(Utils.getGustrowData(me), mBinding.swipeRefresh, mDatabaseMein, TAG, (AppCompatActivity) me, new OnCompleteApi() {
                    @Override
                    public void onComplete() {
                        setFromDB();
                        if (mStarAdapter != null)
                            mStarAdapter.setArray(mArrayTransaction);
                    }
                });

            } else {
                mBinding.swipeRefresh.setRefreshing(false);
                Utils.noInternet(me);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
