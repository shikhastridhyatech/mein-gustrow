package com.meingustrow.app.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.meingustrow.app.R;
import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.base.BaseActivity;
import com.meingustrow.app.util.Utils;
import com.meingustrow.app.webservice.APIs;
import com.meingustrow.app.webservice.JSONCallback;
import com.meingustrow.app.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SplashActivity extends BaseActivity {
    private static final String TAG = SplashActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Utils.getTheme(me));
        setContentView(R.layout.activity_splash);

        if (Utils.getTheme(me) == -1) {
            Utils.setTheme(me, R.style.ThemeLight);
        }
        setData();
    }

    private void setData() {
        Bundle bundle = new Bundle();
        bundle.putString("Splash", "opened");
        FirebaseAnalytics.getInstance(this).logEvent("SplashScreen", bundle);

        try {

            new CountDownTimer(2000, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {

                    Intent intent = new Intent(me, StartActivity.class);
                    startActivity(intent);
                    finish();

                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
