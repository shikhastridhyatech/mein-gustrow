package com.meingustrow.app.ui.fragment;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.meingustrow.app.R;
import com.meingustrow.app.adapter.HomeMainAdapter;
import com.meingustrow.app.adapter.ViewPagerAdapter;
import com.meingustrow.app.broadcast.MyAlarm;
import com.meingustrow.app.databinding.FragmentHomeBinding;
import com.meingustrow.app.interfaces.OnCompleteApi;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.model.WeatherModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.AlertUtils;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private static final String TAG = HomeFragment.class.getName();
    public FragmentHomeBinding mBinding;
    private HomeMainAdapter mHomeMainAdapter;
    private ViewPagerAdapter mViewPagerAdapter;
    private BottomMenuFragment bottomMenuFragment;
    private DatabaseMein mDatabaseMein;
    private List<MainCategoryModel> mArrayMainCategory = new ArrayList<>();
    private List<SubContentModel> mArrayContent = new ArrayList<>();
    private WeatherModel mWeatherModel;
    private ArrayList<Integer> mArraySlider = new ArrayList<>();
    private int currentPage = 0;
    private final long DELAY_MS = 750;
    private final long PERIOD_MS = 3000;

    public HomeFragment() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        try {
            mArraySlider.add(R.drawable.slider_3_new);
            mArraySlider.add(R.drawable.slider_4_new);
            mArraySlider.add(R.drawable.slider_1_new);
            mArraySlider.add(R.drawable.slider_2_new);

            Intent intent = new Intent(me, MyAlarm.class);
            boolean alarmUp = (PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_LOGIN,
                    intent,
                    PendingIntent.FLAG_NO_CREATE) != null);

            Log.e(TAG, "alarmUp............" + alarmUp);
            Log.e(TAG, "getAlarmGustrow........" + Utils.getAlarmLogin(me));

            mBinding.swipeRefresh.setOnRefreshListener(this::onRefresh);
            mBinding.swipeRefresh.setColorSchemeResources(R.color.colorAccent);

            mDatabaseMein = DatabaseMein.getInstance(me);
            setViewPager();
            setDataWithDatabase();
            setWeather();
            if (mArrayMainCategory.size() != 0)
                setAdapter();

            sessionExpiredDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mBinding.getRoot();
    }

    private void sessionExpiredDialog() {
        if (Utils.getAlarmLogin(me)) {
            AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), getString(R.string.session_expired_for_sales_partner), true, "", getString(R.string.ok), new OnConfirmationDialog() {
                @Override
                public void onYes() {
                    try {
                        mDatabaseMein = DatabaseMein.getInstance(me);
                        Utils.setAlarmLogin(me, false);
                        session.logoutUser(me);
                        ApiDatabase.weatherApi(session, mDatabaseMein, TAG, (AppCompatActivity) me, new OnCompleteApi() {
                            @Override
                            public void onComplete() {
                                setWeather();
                            }
                        });

                        ApiDatabase.categoryApi(session, null, mDatabaseMein, "fromFragment",
                                TAG, (AppCompatActivity) me, new OnCompleteApi() {
                                    @Override
                                    public void onComplete() {
                                        setDataWithDatabase();
                                        mHomeMainAdapter.setArray(mArrayMainCategory);
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNo() {
                }
            });
        }

        if (Utils.getAlarmGustrow(me)) {
            try {
                AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), getString(R.string.session_expired_gustrow_card), true, "", getString(R.string.ok), new OnConfirmationDialog() {
                    @Override
                    public void onYes() {
                        try {

                            Utils.setLogInGustrow(me, false);
                            Utils.setAlarmGustrow(me, false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNo() {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void refresh(String dialogString) {
        try {
            Log.e(TAG, "refresh.................");
            ApiDatabase.weatherApi(session, mDatabaseMein, TAG, (AppCompatActivity) me, new OnCompleteApi() {
                @Override
                public void onComplete() {
                    setWeather();
                }
            });
            ApiDatabase.categoryApi(session, null, mDatabaseMein, "fromFragment", TAG, (AppCompatActivity) me, new OnCompleteApi() {
                @Override
                public void onComplete() {
                    setDataWithDatabase();
                }
            });
            setViewPager();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ((DashboardActivity) getActivity()).mBinding.includeHome.frameDashboardMain.setPadding(0, 0, 0, 0);
    }

    private void setDataWithDatabase() {
        try {
            mArrayMainCategory = mDatabaseMein.daoAccess().getAllCategory();
            mArrayContent = mDatabaseMein.daoAccess().getAllContent();
            mArrayMainCategory.remove(0);
            for (int i = 0; i < mArrayMainCategory.size(); i++) {
                MainCategoryModel mainCategoryModel = mArrayMainCategory.get(i);
                String[] items = mainCategoryModel.getItems().split(",");
                for (int k = 0; k < items.length; k++) {
                    for (int j = 0; j < mArrayContent.size(); j++) {
                        if (items[k].equalsIgnoreCase("" + mArrayContent.get(j).getId())) {

                            List<SubContentModel> list = mArrayMainCategory.get(i).getArraySubContnet();
                            if (list == null) {
                                list = new ArrayList<>();
                                list.add(mArrayContent.get(j));
                            } else
                                list.add(mArrayContent.get(j));
                            mArrayMainCategory.get(i).setArraySubContnet(list);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setWeather() {

        mWeatherModel = mDatabaseMein.daoAccess().getWeather();
        try {
            Log.e(TAG, "getWeekday............" + mWeatherModel.getWeekday());
            mBinding.tvHomeDay.setText(mWeatherModel.getWeekday());
            mBinding.tvHomeTemp.setText(mWeatherModel.getData().getTemp() + "\u00B0");
            mBinding.tvHomeDate.setText(mWeatherModel.getDate());

            Glide.with(me)
                    .asBitmap()
                    .load(mWeatherModel.getData().getIcon())
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            mBinding.imgWeather.setImageBitmap(resource);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        try {
            mHomeMainAdapter = new HomeMainAdapter(me, mArrayMainCategory, mDatabaseMein);
            LinearLayoutManager layoutManager = new LinearLayoutManager(me, RecyclerView.VERTICAL, false);
            mBinding.rvHome.setLayoutManager(layoutManager);
            mBinding.rvHome.setAdapter(mHomeMainAdapter);
            mBinding.rvHome.setNestedScrollingEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.e(TAG, "onResume.............");
            if (bottomMenuFragment != null) {
                bottomMenuFragment = null;
            }
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.VISIBLE);
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("Home", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("HomeScreen", bundle);
    }

    @Override
    public void onStop() {
        mBinding.vpHome.stopAutoCycle();
        super.onStop();
    }

    private void setViewPager() {
        try {

            // initialize a SliderLayout
            for (int i = 0; i < mArraySlider.size(); i++) {
                DefaultSliderView textSliderView = new DefaultSliderView(me);
                textSliderView
                        .image(mArraySlider.get(i))
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener(this);
                mBinding.vpHome.addSlider(textSliderView);
            }

            mBinding.vpHome.setDuration(3000);

//            mViewPagerAdapter = new ViewPagerAdapter(me, mArraySlider, true);
//            mBinding.vpHome.setAdapter(mViewPagerAdapter);
//            mBinding.vpHome.addOnPageChangeListener(new CircularViewPagerHandler(mBinding.vpHome));

           /* final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPage == mArraySlider.size()) {
                    currentPage = 0;
                }
                mBinding.vpHome.setCurrentItem(currentPage++, true);
            };

            Timer timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        try {
            if (Utils.isConnectingToInternet(me)) {
                ApiDatabase.weatherApi(session, mDatabaseMein, TAG, (AppCompatActivity) me, new OnCompleteApi() {
                    @Override
                    public void onComplete() {
                        Log.e(TAG, "weatherApi---------------");
                        setWeather();
                    }
                });
                ApiDatabase.categoryApi(session, mBinding.swipeRefresh, mDatabaseMein, "fromFragment", TAG, (AppCompatActivity) me, new OnCompleteApi() {
                    @Override
                    public void onComplete() {
                        Log.e(TAG, "categoryApi---------------");
                        setDataWithDatabase();
                        mHomeMainAdapter.setArray(mArrayMainCategory);
                    }
                });

//                setViewPager();
            } else {
                mBinding.swipeRefresh.setRefreshing(false);
                Utils.noInternet(me);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}
