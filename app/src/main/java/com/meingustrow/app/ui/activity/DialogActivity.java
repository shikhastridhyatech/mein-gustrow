package com.meingustrow.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ActivityBlankBinding;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.base.BaseActivity;
import com.meingustrow.app.ui.fragment.LoginFragment;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import java.util.Objects;

public class DialogActivity extends BaseActivity {

    private static final String TAG = DialogActivity.class.getName();
    private ActivityBlankBinding mBinding;
    private DatabaseMein mDatabaseMein;
    private String whichDialog = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Utils.getTheme(this));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_blank);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mDatabaseMein = DatabaseMein.getInstance(me);

        getFromReceiver();
        this.setFinishOnTouchOutside(false);

    }

    private void getFromReceiver() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                whichDialog = bundle.getString(AppConstants.ALARM_MANAGER_DIALOG);

                if (Objects.requireNonNull(whichDialog).equals(LoginFragment.class.getName())) {
                    mBinding.tvSubTitle.setText(R.string.session_expired_for_sales_partner);
                    showCustomConfirmationDialog(getString(R.string.app_name), getString(R.string.session_expired_for_sales_partner),
                            true, "", getString(R.string.ok));
                } else {
                    mBinding.tvSubTitle.setText(R.string.session_expired_gustrow_card);
                    showCustomConfirmationDialog(getString(R.string.app_name), getString(R.string.session_expired_gustrow_card),
                            true, "", getString(R.string.ok));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCustomConfirmationDialog(String titleText, String subTitleText, boolean showOneButton,
                                             String negativeText, String positiveText) {
        try {
            mBinding.tvTitle.setText(titleText);

            if (!subTitleText.equals("")) {
                mBinding.tvSubTitle.setText(subTitleText);
                mBinding.tvSubTitle.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvSubTitle.setVisibility(View.GONE);
            }

            if (!negativeText.equals("")) {
                mBinding.tvNegative.setText(negativeText);
            }

            if (!positiveText.equals("")) {
                mBinding.tvPositive.setText(positiveText);
            }

            if (showOneButton) {
                mBinding.tvNegative.setVisibility(View.GONE);
                mBinding.viewDialog.setVisibility(View.GONE);
            } else {
                mBinding.tvNegative.setVisibility(View.VISIBLE);
                mBinding.viewDialog.setVisibility(View.VISIBLE);
            }
            mBinding.tvPositive.setOnClickListener(v -> {
                try {
                    if (whichDialog.equals(LoginFragment.class.getName())) {
                        session.logoutUser(me);
                        Utils.setAlarmLogin(me, false);
                    } else {
                        Utils.setLogInGustrow(DialogActivity.this, false);
                        Utils.setAlarmGustrow(me, false);
                        Utils.clearGustrowData(me);
                    }
                    Intent intent = new Intent("refresh");
                    intent.putExtra(AppConstants.ALARM_MANAGER_DIALOG, whichDialog);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            mBinding.tvNegative.setOnClickListener(v -> {
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
    }
}
