package com.meingustrow.app.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.meingustrow.app.R;
import com.meingustrow.app.adapter.SubMenuAdapter;
import com.meingustrow.app.databinding.FragmentSubMenuBinding;
import com.meingustrow.app.interfaces.OnCompleteApi;
import com.meingustrow.app.model.Logo;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class SubMenuFragment extends BaseFragment {

    private static final String TAG = SubMenuFragment.class.getName();
    private FragmentSubMenuBinding mBinding;
    private SubMenuAdapter mSubMenuAdapter;
    private String mSubContent, mDarkLogo, mLightLogo;
    private DatabaseMein mDatabaseMein;
    private List<SubContentModel> mArrayContent = new ArrayList<>();
    private List<String> mArraySubContnet = new ArrayList<>();
    private List<String> mArrayUrls = new ArrayList<>();
    private List<String> mArrayTitle = new ArrayList<>();
    private Logo mLogo;
    private String mTitle;

    public SubMenuFragment(String subContent, Logo logo, String title) {
        this.mSubContent = subContent;
        this.mLogo = logo;
        this.mTitle = title;
    }
    public SubMenuFragment() {
    }
  /*  @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        try {
            if (!AppConstants.eventBusValue) {
                AppConstants.eventBusValue = true;
                ApiDatabase.weatherApi(session, mDatabaseMein, TAG, (AppCompatActivity) me);
                ApiDatabase.categoryApi(session, null, mDatabaseMein, "fromFragment", TAG, (AppCompatActivity) me);

                setDataWithDatabase();
                Log.e(TAG, "result......................................................." + result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    @Override
    public void refresh(String dialogString) {
        try {
            ApiDatabase.categoryApi(session, null, mDatabaseMein, "fromFragment", TAG, (AppCompatActivity) me, new OnCompleteApi() {
                @Override
                public void onComplete() {
                    setDataWithDatabase();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sub_menu, container, false);

        try {
            mDatabaseMein = DatabaseMein.getInstance(me);
            setDataWithDatabase();


            if (mLogo != null && mLogo.getLight() != null && Utils.getTheme(me) == R.style.ThemeLight) {
                Log.e(TAG, "getLight........................" + mLogo.getLight());
                Glide.with(mContext)
                        .asBitmap()
                        .load(mLogo.getLight())
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                mBinding.imgG.setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            } else if (mLogo != null && mLogo.getDark() != null && Utils.getTheme(me) == R.style.ThemeDark) {
                Log.e(TAG, "getDark........................" + mLogo.getDark());
                Glide.with(mContext)
                        .asBitmap()
                        .load(mLogo.getDark())
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                mBinding.imgG.setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            } else {
                mBinding.imgG.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(mTitle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(List<SubContentModel> list) {
        try {
            mSubMenuAdapter = new SubMenuAdapter(me, list, mTitle);
            LinearLayoutManager layoutManager = new LinearLayoutManager(me, RecyclerView.VERTICAL, false);
            mBinding.rvSubMenu.setLayoutManager(layoutManager);
            mBinding.rvSubMenu.setAdapter(mSubMenuAdapter);
            mBinding.rvSubMenu.setNestedScrollingEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataWithDatabase() {
        try {
            List<SubContentModel> list = null;
            mArrayContent = mDatabaseMein.daoAccess().getAllContent();
            String[] items = mSubContent.split(",");
            for (int i = 0; i < items.length; i++) {
                for (int j = 0; j < mArrayContent.size(); j++) {

                    if (items[i].equalsIgnoreCase("" + mArrayContent.get(j).getId())) {
                        if (list == null) {
                            list = new ArrayList<>();
                            list.add(mArrayContent.get(j));
                        } else
                            list.add(mArrayContent.get(j));
                    }
                }
            }
            if (mArrayContent.size() != 0)
                setAdapter(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
