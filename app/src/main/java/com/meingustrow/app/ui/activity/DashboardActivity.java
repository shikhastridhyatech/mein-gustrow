package com.meingustrow.app.ui.activity;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meingustrow.app.BuildConfig;
import com.meingustrow.app.R;
import com.meingustrow.app.adapter.HomeMainAdapter;
import com.meingustrow.app.adapter.MainMenuAdapter;
import com.meingustrow.app.databinding.ActivityDashboardBinding;
import com.meingustrow.app.interfaces.OnRecyclerViewItemClicked;
import com.meingustrow.app.model.Icon;
import com.meingustrow.app.model.Logo;
import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.MenuTitleModel;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.base.BaseActivity;
import com.meingustrow.app.ui.fragment.BottomMenuFragment;
import com.meingustrow.app.ui.fragment.HomeFragment;
import com.meingustrow.app.ui.fragment.LoginFragment;
import com.meingustrow.app.ui.fragment.LoginGustroCardFragment;
import com.meingustrow.app.ui.fragment.SettingsFragment;
import com.meingustrow.app.ui.fragment.SubMenuFragment;
import com.meingustrow.app.ui.fragment.WebFragment;
import com.meingustrow.app.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends BaseActivity implements View.OnClickListener, OnRecyclerViewItemClicked<MenuTitleModel> {

    private static final String TAG = DashboardActivity.class.getName();
    public ActivityDashboardBinding mBinding;
    private HomeMainAdapter mHomeMainAdapter;
    private boolean doubleBackToExitPressedOnce = false;
    private MainMenuAdapter mMenuAdapter;
    private List<Integer> mArrayRedImages = new ArrayList<>();
    private List<Integer> mArrayBlueImages = new ArrayList<>();
    private List<Integer> mArrayWhiteImages = new ArrayList<>();
    private DatabaseMein mDatabaseMein;
    private List<Integer> mIntegersMenu = new ArrayList<>();
    private List<SubContentModel> mArrayMain = new ArrayList<>();
    public int height = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Utils.getTheme(me));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        mBinding.tvVersion.setText(getString(R.string.app_version) + " " + BuildConfig.VERSION_NAME);

        try {
            mDatabaseMein = DatabaseMein.getInstance(me);

            init();
            setDataWithDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataWithDatabase() {
        try {
            List<MainCategoryModel> mArrayMainCategory = mDatabaseMein.daoAccess().getAllCategory();
            List<SubContentModel> mArrayContent = mDatabaseMein.daoAccess().getAllContent();

            if (mArrayMainCategory.size() != 0) {
                for (int i = 0; i < mArrayMainCategory.size(); i++) {
                    //                Log.e(TAG, "getItems............." + mArrayMainCategory.get(i).getItems());

                    if (mArrayMainCategory.get(i).getCategoryId() == 1) {

                        for (String s : mArrayMainCategory.get(0).getItems().split(",")) {
                            mIntegersMenu.add(Integer.valueOf(s.trim()));
                        }
                        Log.e(TAG, "numbers............." + mIntegersMenu);
                    }
                }

                SubContentModel subContentModel = new SubContentModel();
                subContentModel.setTitle(getString(R.string.start_page));
                subContentModel.setUrl("");
                subContentModel.setIcon(new Icon("", ""));
                subContentModel.setLogo(new Logo("", ""));
                subContentModel.setSubContent("");
                mArrayMain.add(subContentModel);

                SubContentModel subContentModel2 = new SubContentModel();
                subContentModel2.setTitle(getString(R.string.gustrow_card));
                subContentModel2.setUrl("");
                subContentModel2.setIcon(new Icon("", ""));
                subContentModel2.setLogo(new Logo("", ""));
                subContentModel2.setSubContent("");
                mArrayMain.add(subContentModel2);
                for (int i = 0; i < mIntegersMenu.size(); i++) {
                    for (int j = 0; j < mArrayContent.size(); j++) {
                        if (mIntegersMenu.get(i).equals(mArrayContent.get(j).getId())) {

                           /* if (mArrayMain == null) {
                                mArrayMain = new ArrayList<>();
                                mArrayMain.add(mArrayContent.get(j));
                            } else*/
                            mArrayMain.add(mArrayContent.get(j));

                           /* if (mArrayContent.get(j).getLogo().getDark() != null) {
                                mArrayDarkLogo.add(mArrayContent.get(j).getLogo().getDark());
                                mArrayLightLogo.add(mArrayContent.get(j).getLogo().getLight());
                            }*/
                        }
                    }
                }
                SubContentModel subContentModel1 = new SubContentModel();
                subContentModel1.setTitle(getString(R.string.settings));
                subContentModel1.setUrl("");
                subContentModel1.setIcon(new Icon("", ""));
                subContentModel1.setLogo(new Logo("", ""));
                subContentModel.setSubContent("");
                mArrayMain.add(subContentModel1);

                setAdapter();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        try {
            mMenuAdapter = new MainMenuAdapter(me, mArrayMain, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(me, RecyclerView.VERTICAL, false);
            mBinding.rvMainMenu.setLayoutManager(layoutManager);
            mBinding.rvMainMenu.setAdapter(mMenuAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        try {
            mBinding.includeHome.includeDashboard.imgNavigationIcon.setOnClickListener(this);
            mBinding.includeHome.includeDashboard.imgNavigationHomeIcon.setOnClickListener(this);
            mBinding.includeHome.includeDashboard.imgBack.setOnClickListener(this);
            mBinding.includeToolbar.imgBack.setOnClickListener(this);

            mBinding.includeToolbar.imgNavigationIcon.setVisibility(View.INVISIBLE);
            mBinding.includeToolbar.tvTitle.setText(R.string.menu);
            setMenuImagesTitle();

            mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.VISIBLE);
            mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.GONE);
            Utils.replaceFragmentDashboard(me, new HomeFragment(), true, true);
            ViewTreeObserver viewTreeObserver = mBinding.includeHome.includeDashboard.getRoot().getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mBinding.includeHome.includeDashboard.getRoot().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        height = mBinding.includeHome.includeDashboard.getRoot().getHeight();
                        Log.e(TAG, "onGlobalLayout: " + height);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClicked(MenuTitleModel menuTitleModel) {
        boolean isSameFragment = mBinding.includeHome.includeDashboard.tvTitle.getText().toString().equalsIgnoreCase(menuTitleModel.getTitleName());
        mBinding.includeHome.includeDashboard.tvTitle.setText(menuTitleModel.getTitleName());
        setPositionClick(menuTitleModel, isSameFragment);
    }

    private void setPositionClick(MenuTitleModel menuTitleModel, boolean isCheck) {
        try {
            mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
            mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);
            mBinding.drawerLayout.closeDrawer(GravityCompat.END);

            if (menuTitleModel.getPosition() == 0) {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    try {
                        Utils.replaceFragmentDashboard(me, new HomeFragment(), true, isCheck);
                        mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.VISIBLE);
                        mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }, 200);
            } else if (menuTitleModel.getPosition() == 1) {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    try {
                        if (Utils.getLogInGustrow(me)) {
                            Utils.replaceFragmentDashboard(me, new BottomMenuFragment(), true, isCheck);
                        } else {
                            Utils.replaceFragmentDashboard(me, new LoginGustroCardFragment(""), true, isCheck);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }, 200);
            } else if (menuTitleModel.getPosition() == (mArrayMain.size() - 1)) {
                try {
                    Utils.replaceFragmentDashboard(me, new SettingsFragment(), true, isCheck);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        try {
                            int position = menuTitleModel.getPosition();
                            if (mArrayMain.get(menuTitleModel.getPosition()).getSubContent() == null) {
                                if (mArrayMain.get(menuTitleModel.getPosition()).getUrl() != null) {
                                    Utils.replaceFragmentDashboard(me, new WebFragment(mArrayMain.get(position).getUrl(), mArrayMain.get(position).getTitle()), true, isCheck);
                                }
                            } else {
//                                Utils.replaceFragmentDashboard(me, new SubMenuFragment(mArraySubContnet.get(position), mArrayDarkLogo.get(position), mArrayLightLogo.get(position)), true);
                                Utils.replaceFragmentDashboard(me, new SubMenuFragment(mArrayMain.get(position).getSubContent(), mArrayMain.get(position).getLogo(), mArrayMain.get(position).getTitle()), true, isCheck);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, 200);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMenuImagesTitle() {
        try {
            @SuppressLint("Recycle") TypedArray tArrayBlue = getResources().obtainTypedArray(R.array.MainBlueIcons);
            int countBlue = tArrayBlue.length();
            int[] iconsBlue = new int[countBlue];

            @SuppressLint("Recycle") TypedArray tArrayRed = getResources().obtainTypedArray(R.array.MainRedIcons);
            @SuppressLint("Recycle") TypedArray tArrayWhite = getResources().obtainTypedArray(R.array.MainWhiteIcons);
            String[] stringNames = getResources().getStringArray(R.array.MainMenuText);

            for (int i = 0; i < iconsBlue.length; i++) {

                int idsBlue = tArrayBlue.getResourceId(i, 0);
                int idsRed = tArrayRed.getResourceId(i, 0);
                int idsWhite = tArrayWhite.getResourceId(i, 0);
            }

            tArrayBlue.recycle();
            tArrayRed.recycle();
            tArrayWhite.recycle();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgNavigationIcon:

                try {

                    mBinding.drawerLayout.openDrawer(GravityCompat.END);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.imgBack:
                try {
                    if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.END)) {
                        mBinding.drawerLayout.closeDrawer(GravityCompat.END);
                    } else {
                        onBackStack();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.imgNavigationHomeIcon:
                try {
                   /* if (Utils.isConnectingToInternet(me)) {
                        categoryApi();
                    }*/
                    mBinding.drawerLayout.openDrawer(GravityCompat.END);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        onBackStack();
    }

    private void onBackStack() {
        try {
            if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.END)) {
                mBinding.drawerLayout.closeDrawer(GravityCompat.END);
            } else {
                final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameDashboardMain);

                FragmentManager fm = getSupportFragmentManager();
                int count = fm.getBackStackEntryCount();
                Log.e(TAG, "count............" + count);

                if (fragment instanceof HomeFragment) {
                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed();
                        finishAffinity();
                        return;
                    }

                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(me, getString(R.string.click_again_to_exit), Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                } else if (fragment instanceof BottomMenuFragment) {

                    if (fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                        fragment.getChildFragmentManager().popBackStackImmediate();
                    } else {
                        Utils.replaceFragmentDashboard(me, new HomeFragment(), true, true);
                        mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.VISIBLE);
                        mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.GONE);
                    }
                    mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
                    mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);
                } else if (fragment instanceof WebFragment) {
                    try {
                        if (WebFragment.mBinding.webView.canGoBack()) {
                            WebFragment.mBinding.webView.goBack();
                        } else {
                            super.onBackPressed();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (fragment instanceof LoginFragment) {
                    super.onBackPressed();
                } else if (fragment instanceof LoginGustroCardFragment) {

                    mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
                    mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);
                    super.onBackPressed();
//                    Utils.replaceFragmentDashboard(me, new HomeFragment(), true);
                } else {
                    Utils.replaceFragmentDashboard(me, new HomeFragment(), true, true);
                    mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.VISIBLE);
                    mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.GONE);
//                    mMenuAdapter.setSelection(getString(R.string.home));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
