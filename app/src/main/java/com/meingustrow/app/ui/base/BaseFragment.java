package com.meingustrow.app.ui.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.snackbar.Snackbar;
import com.meingustrow.app.ApplicationClass;
import com.meingustrow.app.R;
import com.meingustrow.app.dialogs.ProgressDialog;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.interfaces.OnSnackBarActionListener;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.fragment.BottomMenuFragment;
import com.meingustrow.app.ui.fragment.LoginGustroCardFragment;
import com.meingustrow.app.util.AlertUtils;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.GlideUtils;
import com.meingustrow.app.util.SessionManager;
import com.meingustrow.app.util.Utils;

import java.util.Objects;

public class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getName();
    public Context mContext;
    public Activity mActivity;
    public TextView title;
    public SessionManager session;
    private Snackbar snackbar;
    protected GlideUtils glideUtils;
    private ProgressDialog dialog;
    public setPermissionListener permissionListener;
    public Activity me;
    private DatabaseMein mDatabaseMein;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void refresh(String dialogString) {
        try {
            final Fragment fragment = ((DashboardActivity) me).getSupportFragmentManager().findFragmentById(R.id.frameDashboardMain);

            if (fragment instanceof BottomMenuFragment) {
                new Handler().post(new Runnable() {
                    public void run() {
                   /* if (fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                        for (int i = 0; i < fragment.getChildFragmentManager().getBackStackEntryCount(); i++) {
                            fragment.getChildFragmentManager().popBackStackImmediate();
                        }
                    }*/
                        if (dialogString.equals(LoginGustroCardFragment.class.getName())) {
                            Utils.replaceChildFragment((AppCompatActivity) me, new LoginGustroCardFragment(""), true);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String dialogString = intent.getStringExtra(AppConstants.ALARM_MANAGER_DIALOG);
                refresh(dialogString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        me.setTheme(Utils.getTheme(me));
        Log.e(TAG, "getAlarmGustrow................." + Utils.getAlarmGustrow(me));
        final Fragment fragment = ((DashboardActivity) me).getSupportFragmentManager().findFragmentById(R.id.frameDashboardMain);

    }

    /*@Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }*/

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mContext = context;
            me = getActivity();
            LocalBroadcastManager.getInstance(mContext).registerReceiver(broadcastReceiver, new IntentFilter("refresh"));

            mActivity = getActivity();
            session = new SessionManager(mContext);
            glideUtils = new GlideUtils(ApplicationClass.getAppContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setupToolBar(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(Title != null ? Title : "");
    }

    public void setupToolBarWithBackArrow(Toolbar toolbar) {
        setupToolBarWithBackArrow(toolbar, null);
    }

    public void setupToolBarWithBackArrow(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_vector_back);
        }
        toolbar.setNavigationOnClickListener(view -> mActivity.onBackPressed());
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(Title != null ? Title : "");
    }

    public void updateToolBarTitle(@Nullable String Title) {
        title.setText(Title != null ? Title : mContext.getResources().getString(R.string.app_name));
    }

    // Set false for Tab Back press management. It will manage from MainActivity
    public boolean onBackPressed() {
        return false;
    }

    public void showShortToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    public void showSnackBar(View view, String msg) {
        showSnackBar(view, msg, Snackbar.LENGTH_SHORT);
    }

    public void showSnackBar(View view, String msg, int LENGTH) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        snackbar.show();
    }

    public void showSnackBar(View view, String msg, int LENGTH, String action, final OnSnackBarActionListener actionListener) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        snackbar.setActionTextColor(ContextCompat.getColor(mContext, R.color.red));
        if (actionListener != null) {
            snackbar.setAction(action, view1 -> {
                snackbar.dismiss();
                actionListener.onAction();
            });
        }
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        snackbar.show();
    }

    public void showSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void hideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            if (mActivity.getWindow().getCurrentFocus() != null)
                imm.hideSoftInputFromWindow(mActivity.getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ProgressDialog showProgressBar() {
        return showProgressBar(null);
    }

    public ProgressDialog showProgressBar(String message) {
        if (dialog == null) dialog = new ProgressDialog(mContext, message);
        return dialog;
    }

    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void requestAppPermissions(final String[] requestedPermissions, final int requestCode,
                                      setPermissionListener listener) {
        this.permissionListener = listener;
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (String permission : requestedPermissions) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(mActivity, permission);
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(requestedPermissions, requestCode);
        } else {
            if (permissionListener != null) permissionListener.onPermissionGranted(requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }
        if ((grantResults.length > 0) && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            if (permissionListener != null) permissionListener.onPermissionGranted(requestCode);
        } else {
            if (permissionListener != null) permissionListener.onPermissionDenied(requestCode);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
//        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(broadcastReceiver);
        if (snackbar != null && snackbar.isShown()) snackbar.dismiss();
    }

    public interface setPermissionListener {
        void onPermissionGranted(int requestCode);

        void onPermissionDenied(int requestCode);
    }

    /*protected void pushFragment(Fragment fragment) {
        if (mActivity instanceof VirtualBankActivity) {
            pushFragment(((VirtualBankActivity) mActivity).mCurrentTab, fragment, true, true);
        } else if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).pushFragment(fragment);
        }
    }

    protected void pushFragment(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (mActivity instanceof VirtualBankActivity) {
            ((VirtualBankActivity) mActivity).pushFragment(tag, fragment, shouldAnimate, shouldAdd);
        }
    }

    protected void clearBackStackInclusive() {
        if (mActivity instanceof VirtualBankActivity) {
            clearBackStackInclusive(false);
        } else if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).clearBackStackInclusive(true);
        }
    }

    protected void clearBackStackInclusive(boolean isRedirectToHomeTabOrFinishActivity) {
        if (mActivity instanceof VirtualBankActivity) {
            ((VirtualBankActivity) mActivity).clearBackStackInclusive(isRedirectToHomeTabOrFinishActivity);
        } else if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).clearBackStackInclusive(isRedirectToHomeTabOrFinishActivity);
        }
    }

    protected void clearBackStackInclusive(String tag) {
        if (mActivity instanceof VirtualBankActivity) {
            ((VirtualBankActivity) mActivity).clearBackStackInclusive(tag);
        } else if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).clearBackStackInclusive(tag);
        }
    }*/
}