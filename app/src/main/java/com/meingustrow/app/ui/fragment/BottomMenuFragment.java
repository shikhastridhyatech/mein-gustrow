package com.meingustrow.app.ui.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.meingustrow.app.R;
import com.meingustrow.app.broadcast.MyAlarm;
import com.meingustrow.app.databinding.FragmentBottomMenuBinding;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.AlertUtils;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class BottomMenuFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = BottomMenuFragment.class.getName();
    private QRCodeFragment mQrCodeFragment = new QRCodeFragment();
    private StarFragment mStarFragment = new StarFragment();
    private OffersFragment mOffersFragment = new OffersFragment();
    private PartnerFragment mPartnerFragment = new PartnerFragment();
    private ProfileFragment mProfileFragment = new ProfileFragment();
    private Fragment active = mQrCodeFragment;
    public static FragmentBottomMenuBinding mBinding;

    public BottomMenuFragment() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_menu, container, false);

        try {
            mBinding.bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

            if (mQrCodeFragment == null) {
                mQrCodeFragment = new QRCodeFragment();
            }
            getChildFragmentManager().beginTransaction().replace(R.id.frameBottomMenu, mQrCodeFragment).commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity) Objects.requireNonNull(mContext)).mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
        ((DashboardActivity) Objects.requireNonNull(mContext)).mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);
        ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.gustrow_card);
    }

    private void setFragment(Fragment fragment, String TAG) {
        getChildFragmentManager().beginTransaction().replace(R.id.frameBottomMenu, fragment, TAG).addToBackStack(TAG).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        try {

            switch (item.getItemId()) {

                case R.id.navi_card:
                    setFragment(mQrCodeFragment, QRCodeFragment.class.getName());
                    break;

                case R.id.navi_star:
                    setFragment(mStarFragment, StarFragment.class.getName());
             /*       if (mStarFragment != null) {
                        mStarFragment.onRefresh();
                    }*/
                    break;

                case R.id.nav_partner:
                    setFragment(mPartnerFragment, PartnerFragment.class.getName());

                    break;

                case R.id.nav_logout:
                    try {
                        AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), getString(R.string.want_to_log_out), false, getString(R.string.no), getString(R.string.yes), new OnConfirmationDialog() {
                            @Override
                            public void onYes() {

                                try {
                                    Intent intent = new Intent(me, MyAlarm.class);
                                    boolean alarmUp = (PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_GUSTROW,
                                            intent,
                                            PendingIntent.FLAG_NO_CREATE) != null);

                                    Log.e(TAG, "alarmUp............" + alarmUp);
                                    if (alarmUp) {
                                        PendingIntent pendingIntent = PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_GUSTROW, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                        AlarmManager alarmManager = (AlarmManager) me.getSystemService(Context.ALARM_SERVICE);
                                        pendingIntent.cancel();
                                        assert alarmManager != null;
                                        alarmManager.cancel(pendingIntent);
                                    }
                                    DatabaseMein.getInstance(me).daoAccess().deleteTransaction();

                                    Utils.setLogInGustrow(me, false);
                                    Utils.setAlarmGustrow(me, false);
                                    Utils.clearGustrowData(me);

                                    Intent intent1 = new Intent("refresh");
                                    intent1.putExtra(AppConstants.ALARM_MANAGER_DIALOG, "");
                                    LocalBroadcastManager.getInstance(me).sendBroadcast(intent1);
                                    startActivity(new Intent(me, DashboardActivity.class));
                                    me.finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNo() {
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

               /* case R.id.nav_offers:
                    setFragment(mOffersFragment, OffersFragment.class.getName());
                    break;

                case R.id.nav_profile:
                    setFragment(mProfileFragment, ProfileFragment.class.getName());
                    break;*/
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    };

    public void changeFragment(Fragment fragment, String tagFragmentName) {
        FragmentManager mFragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        /*Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }*/

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frameBottomMenu, fragmentTemp, tagFragmentName);
        } else {
            fragmentTransaction.detach(mFragmentManager.getPrimaryNavigationFragment());
            fragmentTransaction.attach(fragment);
//            fragmentTransaction.show(fragmentTemp);
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
//        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNow();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }
}
