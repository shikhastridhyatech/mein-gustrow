package com.meingustrow.app.ui.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.meingustrow.app.R;
import com.meingustrow.app.broadcast.MyAlarm;
import com.meingustrow.app.databinding.ActivitySettingsBinding;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.AlertUtils;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = SettingsFragment.class.getName();
    private ActivitySettingsBinding mBinding;

    public SettingsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.activity_settings, container, false);

        try {
            setTheme();

            if (session.isLoggedIn()) {
                mBinding.tvLoginSettings.setText(R.string.logout);
            } else {
                mBinding.tvLoginSettings.setText(R.string.sales_login);
            }
            mBinding.tvLoginSettings.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mBinding.getRoot();
    }

    @Override
    public void refresh(String dialogString) {
        try {
            Log.e(TAG, "refresh.................");
            if (session.isLoggedIn()) {
                mBinding.tvLoginSettings.setText(R.string.logout);
            } else {
                mBinding.tvLoginSettings.setText(R.string.sales_login);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTheme() {
        try {
            if (Utils.getTheme(me) == R.style.ThemeLight) {
                mBinding.switchButton.setChecked(true);
            } else {
                mBinding.switchButton.setChecked(false);
            }

            mBinding.switchButton.setOnCheckedChangeListener((view, isChecked) -> {
                if (isChecked) {
                    Utils.setTheme(me, R.style.ThemeLight);
                } else {
                    Utils.setTheme(me, R.style.ThemeDark);
                }
                recreateActivity();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void recreateActivity() {
        try {
            Intent intent = new Intent(me, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            me.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.settings);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("Settings", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("SettingsScreen", bundle);
    }

  /*  @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Log.e(TAG, "result......................................................." + result);
        }
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvLoginSettings:
                try {
                    if (mBinding.tvLoginSettings.getText().toString().equalsIgnoreCase(getString(R.string.sales_login))) {
                        Utils.replaceFragmentDashboard((DashboardActivity) me, new LoginFragment(getString(R.string.from_settings)), true, true);
                        ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.sales_login);
                    } else {
                        AlertUtils.showCustomConfirmationDialog(me, getString(R.string.app_name), getString(R.string.want_to_log_out), false, getString(R.string.no), getString(R.string.yes), new OnConfirmationDialog() {
                            @Override
                            public void onYes() {
                                try {
                                    Intent intent = new Intent(me, MyAlarm.class);
                                    boolean alarmUp = (PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_LOGIN,
                                            intent,
                                            PendingIntent.FLAG_NO_CREATE) != null);

                                    Log.e(TAG, "alarmUp............" + alarmUp);
                                    if (alarmUp) {
                                        PendingIntent pendingIntent = PendingIntent.getBroadcast(me, AppConstants.REQUEST_CODE_LOGIN, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                        AlarmManager alarmManager = (AlarmManager) me.getSystemService(Context.ALARM_SERVICE);
                                        pendingIntent.cancel();
                                        assert alarmManager != null;
                                        alarmManager.cancel(pendingIntent);
                                    }

                                    session.logoutUser(me);
                                    Utils.setAlarmLogin(me, false);

                                    mBinding.tvLoginSettings.setText(R.string.sales_login);
                                    Intent intent1 = new Intent("refresh");
                                    intent1.putExtra(AppConstants.ALARM_MANAGER_DIALOG, "");
                                    LocalBroadcastManager.getInstance(me).sendBroadcast(intent1);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNo() {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
