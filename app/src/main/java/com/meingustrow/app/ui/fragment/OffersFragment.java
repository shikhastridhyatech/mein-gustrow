package com.meingustrow.app.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.FragmentOffersBinding;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.Utils;
import com.meingustrow.app.webservice.APIs;

import java.util.Objects;

public class OffersFragment extends BaseFragment {

    private static final String TAG = OffersFragment.class.getName();
    private FragmentOffersBinding mBinding;

    public OffersFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false);

//        if (Utils.isConnectingToInternet(me)) {
//            Utils.showProgressBar(me);

        try {
            mBinding.webView.getSettings().setAllowContentAccess(true);
            mBinding.webView.getSettings().setAllowFileAccess(true);
            mBinding.webView.getSettings().setAllowFileAccessFromFileURLs(true);
            mBinding.webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            mBinding.webView.getSettings().setJavaScriptEnabled(true);
            mBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mBinding.webView.setClickable(true);
            mBinding.webView.setWebChromeClient(new WebChromeClient());
            mBinding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

            mBinding.webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    //                    Utils.dismissProgressBar();
                }
            });

            mBinding.webView.loadUrl(APIs.TEMP_WEB);
//        } else {
//            Toast.makeText(mContext, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mBinding.getRoot();
    }

   /* @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;

            Objects.requireNonNull(((DashboardActivity) me)).getSupportFragmentManager().popBackStack();
            Utils.replaceFragmentDashboard((AppCompatActivity) me, new LoginFragment(""), true);

            Log.e(TAG, "result......................................................." + result);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        try {
            BottomMenuFragment.mBinding.bottomNavigation.getMenu().getItem(3).setChecked(true);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.imgNavigationIcon.setVisibility(View.VISIBLE);
            ((DashboardActivity) Objects.requireNonNull(getActivity())).mBinding.includeHome.includeDashboard.tvLogout.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
