package com.meingustrow.app.ui.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.meingustrow.app.R;
import com.meingustrow.app.Retrofit.ApiRetrofit;
import com.meingustrow.app.broadcast.MyAlarm;
import com.meingustrow.app.databinding.FragmentLoginBinding;
import com.meingustrow.app.model.UserModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.base.BaseFragment;
import com.meingustrow.app.util.ApiDatabase;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = LoginFragment.class.getName();
    private FragmentLoginBinding mBinding;
    public String mString = "";
    private DatabaseMein mDatabaseMein;

    public LoginFragment(String string) {
        this.mString = string;
    }

    public LoginFragment() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        me.setTheme(Utils.getTheme(me));
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

//        mBinding.edtEmail.setText("test@gmail.com");
//        mBinding.edtPassword.setText("test@gmail.com");

        try {
            mDatabaseMein = DatabaseMein.getInstance(me);

            mBinding.tvLogin.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mBinding.getRoot();
    }

   /* @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResultReceived(String result) {
        if (!AppConstants.eventBusValue) {
            AppConstants.eventBusValue = true;
            Log.e(TAG, "result......................................................." + result);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (mString.equalsIgnoreCase(getString(R.string.from_settings))) {
                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.login);
            } else {
                ((DashboardActivity) me).mBinding.includeHome.includeDashboard.tvTitle.setText(R.string.gustrow_card);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putString("Login", "opened");
        FirebaseAnalytics.getInstance(getActivity()).logEvent("LoginScreen", bundle);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvLogin:
                View parentLayout = getActivity().findViewById(android.R.id.content);
                try {
                    if (Objects.requireNonNull(mBinding.edtEmail.getText()).toString().trim().equals("")) {
                        showSnackBar(parentLayout, getString(R.string.please_enter_email));
                    } else if (!mBinding.edtEmail.getText().toString().trim().matches(AppConstants.emailPattern)) {
                        showSnackBar(parentLayout, getString(R.string.please_enter_valid_email));
                    } else if (mBinding.edtPassword.getText().toString().trim().equals("")) {
                        showSnackBar(parentLayout, getString(R.string.please_enter_password));
                    } /*else if (mBinding.edtPassword.getText().toString().trim().length() < 8) {
                        showSnackBar(parentLayout, getString(R.string.plese_enter_min_8_length));
                    } else if (mBinding.edtPassword.getText().toString().trim().length() > 16) {
                        showSnackBar(parentLayout, getString(R.string.plese_enter_less_16_length));
                    } else if (!mBinding.edtPassword.getText().toString().trim().matches(AppConstants.passwordPattern)) {
                        showSnackBar(parentLayout, getString(R.string.please_enter_valid_password));
                    }*/ else {
                        if (Utils.isConnectingToInternet(me)) {
                            signinApi();
                        } else {
                            Utils.noInternet(me);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void signinApi() {
        String md5String = Utils.convertMD5(Objects.requireNonNull(mBinding.edtPassword.getText()).toString().trim());
        Dialog dialog = showProgressBar();
        dialog.show();
        Call<ResponseBody> call = ApiRetrofit.getClient().
                signInUser(Objects.requireNonNull(mBinding.edtEmail.getText()).toString().trim(), md5String);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    hideProgressBar();

                    String id = null;
                    try {
                        assert response.body() != null;
                        String responseString = response.body().string();
                        Log.e(TAG, "signin--------------" + responseString);
                        JSONObject jsonObject = new JSONObject(responseString);

                        Type modelType = new TypeToken<UserModel>() {
                        }.getType();
                        UserModel userData = new Gson().fromJson(jsonObject.toString(), modelType);

                        id = jsonObject.getString("id");

                        if (id.equalsIgnoreCase("null")) {
                            Utils.dialodWithSingleButton(me, getString(R.string.email_or_psw_wrong));
                        } else {
                            Log.e(TAG, "isChecked..................." + mBinding.cbLogin.isChecked());
                            if (!mBinding.cbLogin.isChecked()) {
                                new MyAlarm(me, 1800, LoginFragment.class.getName(), AppConstants.REQUEST_CODE_LOGIN);
                            }
                            session.storeUserDetail(userData);
                            if (mString.equalsIgnoreCase(getString(R.string.from_settings))) {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack();
                            } else {
                                Utils.replaceFragmentDashboard((AppCompatActivity) me, new BottomMenuFragment(), true, true);
                            }

                            ApiDatabase.weatherApi(session, mDatabaseMein, TAG, (AppCompatActivity) me, null);
                            ApiDatabase.categoryApi(session, null, mDatabaseMein, "fromFragment", TAG, (AppCompatActivity) me, null);
                        }
                        Log.e(TAG, "id................." + session.getUserDetail().getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "call...." + call.toString());
                hideProgressBar();
            }
        });
    }
/*
    private void loginApi() {
        try {
            String md5String = Utils.convertMD5(mBinding.edtPassword.getText().toString().trim());
            HashMap<String, String> params = new HashMap<>();
            params.put("email", mBinding.edtEmail.getText().toString().trim());
            params.put("password", md5String);

            HashMap<String, String> header = new HashMap<>();
            params.put("Content-Type", "application/x-www-form-urlencoded");

            try {
                Retrofit.with(me)
                        .setAPI(APIs.LOGIN)
                        .setHeader(header)
                        .setParameters(params)
                        .setCallBackListener(new JSONCallback(TAG, me, showProgressBar()) {
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) {
                                hideProgressBar();
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            protected void onFailed(int statusCode, String errorMessage) {
                                hideProgressBar();
                            }

                            @Override
                            protected void onFailure(String failureMessage) {
                                hideProgressBar();
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
                hideProgressBar();
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressBar();
        }
    }
    */
}
