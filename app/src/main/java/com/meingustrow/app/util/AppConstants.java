package com.meingustrow.app.util;

public class AppConstants {

    public static final String BASE_DOMAIN = "https://www.mgapp.psnmedia.cloud/";   //Client

    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*^#~?&])[A-Za-z\\d@$!%*^#~?&]{8,}$";
    public static final int REQUEST_CODE_GUSTROW = 10;
    public static final int REQUEST_CODE_LOGIN = 20;

    public static boolean eventBusValue = false;
    public static String ALARM_MANAGER_CLASS = "alarmManagerClass";
    public static String ALARM_MANAGER_DIALOG = "alarmManagerDialog";

    // Image Selection
    public static final int CAMERA = 0x50;
    public static final int GALLERY = 0x51;

    //Drawable Sides
    public static final int DRAWABLE_LEFT = 0x29;
    public static final int DRAWABLE_RIGHT = 0x2A;
    public static final int DRAWABLE_TOP = 0x2B;
    public static final int DRAWABLE_BOTTOM = 0x2C;

}
