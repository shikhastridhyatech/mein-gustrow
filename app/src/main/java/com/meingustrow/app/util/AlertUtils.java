package com.meingustrow.app.util;

import android.app.Activity;
import android.app.Dialog;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.DialogAlertCustomBinding;
import com.meingustrow.app.interfaces.OnConfirmationDialog;

public class AlertUtils {

    public static void showCustomConfirmationDialog(final Activity context, String titleText, String subTitleText, boolean showOneButton, String negativeText, String positiveText, final OnConfirmationDialog onConfirmationDialog) {
        try {
            final Dialog dialog = new Dialog(context);
            final DialogAlertCustomBinding binding = DataBindingUtil.inflate(context.getLayoutInflater(), R.layout.dialog_alert_custom, null, false);
            dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
            dialog.setContentView(binding.getRoot());
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.animationdialog;

            binding.tvTitle.setText(titleText);

            if (!subTitleText.equals("")) {
                binding.tvSubTitle.setText(subTitleText);
                binding.tvSubTitle.setVisibility(View.VISIBLE);
            } else {
                binding.tvSubTitle.setVisibility(View.GONE);
            }

            if (!negativeText.equals("")) {
                binding.tvNegative.setText(negativeText);
            }

            if (!positiveText.equals("")) {
                binding.tvPositive.setText(positiveText);
            }

            if (showOneButton) {
                binding.tvNegative.setVisibility(View.GONE);
                binding.viewDialog.setVisibility(View.GONE);
            } else {
                binding.tvNegative.setVisibility(View.VISIBLE);
                binding.viewDialog.setVisibility(View.VISIBLE);
            }

            binding.tvPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (onConfirmationDialog != null) {
                        onConfirmationDialog.onYes();
                    }
                }
            });

            binding.tvNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (onConfirmationDialog != null) {
                        onConfirmationDialog.onNo();
                    }
                }
            });

            dialog.show();
            WindowManager.LayoutParams windowManager = context.getWindow().getAttributes();
            windowManager.dimAmount = 0.2f;
            context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
