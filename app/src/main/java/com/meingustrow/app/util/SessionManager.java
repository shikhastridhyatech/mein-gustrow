package com.meingustrow.app.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.meingustrow.app.R;
import com.meingustrow.app.model.UserModel;


public class SessionManager {

    public static final String KEY_LANGUAGE = "language";
    public static final String IS_LOGIN = "is_login";
    public static final String KEY_CURRENCY = "user_currency";
    private static final String KEY_USER_MODEL = "user_model";
    private static final String KEY_PREF = "KEY_PREF";
    private SharedPreferences pref;
    private Context mContext;
    SharedPreferences.Editor editor;

    public SessionManager(Context context) {
        String PREF_NAME = context.getResources().getString(R.string.app_name);
//        pref = SecurePreferences.getInstance(context, PREF_NAME);
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        mContext = context;
    }

    public void storeUserDetail(UserModel userModel) {
        storeUserDetail(userModel, true);
    }

    public void storeUserDetail(UserModel userModel, boolean isLogin) {
        Gson gson = new Gson();
        String json = gson.toJson(userModel); // myObject - instance of UserModel
        editor.putString(KEY_USER_MODEL, json);
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.commit();
    }

    public UserModel getUserDetail() {
        Gson gson = new Gson();
        String json = pref.getString(KEY_USER_MODEL, null);
        return gson.fromJson(json, UserModel.class);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getDataByKey(String Key) {
        return getDataByKey(Key, "");
    }

    public String getDataByKey(String Key, String DefaultValue) {
        String returnValue;
        if (pref.contains(Key)) {
            returnValue = pref.getString(Key, DefaultValue);
        } else {
            returnValue = DefaultValue;
        }
        return returnValue;
    }

    public Boolean getDataByKey(String Key, boolean DefaultValue) {
        if (pref.contains(Key)) {
            return pref.getBoolean(Key, DefaultValue);
        } else {
            return DefaultValue;
        }
    }

    public int getDataByKey(String Key, int DefaultValue) {
        if (pref.contains(Key)) {
            return pref.getInt(Key, DefaultValue);
        } else {
            return DefaultValue;
        }
    }

    public void storeDataByKey(String key, int Value) {
        editor.putInt(key, Value);
        editor.commit();
    }

    public void storeDataByKey(String key, String Value) {
        editor.putString(key, Value);
        editor.commit();
    }

    public void storeDataByKey(String key, boolean Value) {
        editor.putBoolean(key, Value);
        editor.commit();
    }

    public void clearDataByKey(String key) {
        editor.remove(key);
    }

    // region Helper Methods
    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);
    }

    // Reset Challenge
    public void logoutUser(Context context) {
        storeDataByKey(SessionManager.IS_LOGIN, false);
//        storeDataByKey(SessionManager.KEY_USER_MODEL, null);
        SharedPreferences.Editor editor = getEditor(context);
        editor.remove(KEY_USER_MODEL)
                .apply();

        // Redirect to Welcome Screen
       /* Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("BlankIntent", "BlankIntent");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();*/
    }
}
