package com.meingustrow.app.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.meingustrow.app.R;
import com.meingustrow.app.Retrofit.ApiRetrofit;
import com.meingustrow.app.databinding.DialogAlertCustomBinding;
import com.meingustrow.app.interfaces.OnCompleteApi;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.model.GustrowLoginModel;
import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.StarHeaderModel;
import com.meingustrow.app.model.StarTransaction;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.model.WeatherModel;
import com.meingustrow.app.room.DatabaseMein;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.webservice.APIs;
import com.meingustrow.app.webservice.JSONCallback;
import com.meingustrow.app.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ApiDatabase {

    private static Dialog dialog;
    static List<StarTransaction> arrayStarTransactions = new ArrayList<>();

    private static class InsertCategory extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<AppCompatActivity> activityReference;
        private List<MainCategoryModel> mArray;
        private DatabaseMein mDatabaseMein;
        private OnCompleteApi listener;

        // only retain a weak reference to the activity
        InsertCategory(AppCompatActivity context, List<MainCategoryModel> array, DatabaseMein databaseMein, OnCompleteApi listener) {
            activityReference = new WeakReference<>(context);
            mArray = array;
            mDatabaseMein = databaseMein;
            this.listener = listener;
        }

        // doInBackground methods runs on a worker thread
        @Override
        protected Boolean doInBackground(Void... objs) {
            mDatabaseMein.daoAccess().insertCategory(mArray);
            return true;
        }

        // onPostExecute runs on main thread
        @Override
        protected void onPostExecute(Boolean bool) {
            if (bool) {
                if (listener != null)
                    listener.onComplete();
                activityReference.get();
            }
        }
    }

    private static class InsertStarTransaction extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<AppCompatActivity> activityReference;
        private List<StarTransaction> mArray;
        private DatabaseMein mDatabaseMein;
        private OnCompleteApi listener;

        // only retain a weak reference to the activity
        InsertStarTransaction(AppCompatActivity context, List<StarTransaction> array, DatabaseMein databaseMein, OnCompleteApi listener) {
            activityReference = new WeakReference<>(context);
            mArray = array;
            mDatabaseMein = databaseMein;
            this.listener = listener;
        }

        // doInBackground methods runs on a worker thread
        @Override
        protected Boolean doInBackground(Void... objs) {
            try {
                mDatabaseMein.daoAccess().deleteTransaction();

                mDatabaseMein.daoAccess().insertStarTransaction(mArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        // onPostExecute runs on main thread
        @Override
        protected void onPostExecute(Boolean bool) {
            if (bool) {
                if (listener != null)
                    listener.onComplete();
                activityReference.get();
            }
        }
    }

    private static class InsertStarHeader extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<AppCompatActivity> activityReference;
        private StarHeaderModel model;
        private DatabaseMein mDatabaseMein;
        private OnCompleteApi listener;

        // only retain a weak reference to the activity
        InsertStarHeader(AppCompatActivity context, StarHeaderModel model, DatabaseMein databaseMein, OnCompleteApi listener) {
            activityReference = new WeakReference<>(context);
            this.model = model;
            mDatabaseMein = databaseMein;
            this.listener = listener;
        }

        // doInBackground methods runs on a worker thread
        @Override
        protected Boolean doInBackground(Void... objs) {
            mDatabaseMein.daoAccess().insertStarHeader(model);
            return true;
        }

        // onPostExecute runs on main thread
        @Override
        protected void onPostExecute(Boolean bool) {
            if (bool) {
                if (listener != null)
                    listener.onComplete();
                activityReference.get();
            }
        }
    }

    private static class InsertWeather extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<AppCompatActivity> activityReference;
        private WeatherModel mWeatherArray;
        private DatabaseMein mDatabaseMein;
        private OnCompleteApi listener;

        // only retain a weak reference to the activity
        InsertWeather(AppCompatActivity context, WeatherModel weatherArray, DatabaseMein databaseMein, OnCompleteApi listener) {
            activityReference = new WeakReference<>(context);
            mWeatherArray = weatherArray;
            mDatabaseMein = databaseMein;
            this.listener = listener;
        }

        // doInBackground methods runs on a worker thread
        @Override
        protected Boolean doInBackground(Void... objs) {
            mDatabaseMein.daoAccess().insertWeather(mWeatherArray);
            return true;
        }

        // onPostExecute runs on main thread
        @Override
        protected void onPostExecute(Boolean bool) {
            if (bool) {
                activityReference.get();
                if (listener != null)
                    listener.onComplete();
            }
        }
    }

    private static class InsertContent extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<AppCompatActivity> activityReference;
        private List<SubContentModel> mCategoryArray;
        private DatabaseMein mDatabaseMein;
        private OnCompleteApi listener;

        // only retain a weak reference to the activity
        InsertContent(AppCompatActivity context, List<SubContentModel> categoryArray, DatabaseMein databaseMein, OnCompleteApi listener) {
            activityReference = new WeakReference<>(context);
            mCategoryArray = categoryArray;
            mDatabaseMein = databaseMein;
            this.listener = listener;
        }

        // doInBackground methods runs on a worker thread
        @Override
        protected Boolean doInBackground(Void... objs) {
            mDatabaseMein.daoAccess().insertContent(mCategoryArray);
            return true;
        }

        // onPostExecute runs on main thread
        @Override
        protected void onPostExecute(Boolean bool) {
            if (bool) {
                activityReference.get();
                if (listener != null)
                    listener.onComplete();
            }
        }
    }

    public static void weatherApi(SessionManager session, DatabaseMein databaseMein, String TAG, AppCompatActivity activity, OnCompleteApi listener) {
        try {
            HashMap<String, String> params = new HashMap<>();
            if (session.isLoggedIn()) {
                params.put("id", "" + session.getUserDetail().getId());
            }
            try {
                Retrofit.with(activity)
                        .setAPI(APIs.WEATHER)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(TAG, activity) {
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) {
                                try {

                                    try {
                                        Type modelType = new TypeToken<WeatherModel>() {
                                        }.getType();
                                        WeatherModel mWeatherModel = new Gson().fromJson(jsonObject.toString(), modelType);

                                        mWeatherModel.setUniqueId(1);
                                        new InsertWeather(activity, mWeatherModel, databaseMein, listener).execute();

                                    } catch (JsonSyntaxException e) {
                                        e.printStackTrace();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            protected void onFailed(int statusCode, String errorMessage) {
                            }

                            @Override
                            protected void onFailure(String failureMessage) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void categoryApi(SessionManager session, SwipeRefreshLayout swipeRefresh, DatabaseMein databaseMein,
                                   String string, String TAG, AppCompatActivity activity, OnCompleteApi listener) {
        try {
            HashMap<String, String> params = new HashMap<>();
            if (session.isLoggedIn()) {
                params.put("id", "" + session.getUserDetail().getId());
            }
            try {
                Retrofit.with(activity)
                        .setAPI(APIs.CATEGORY)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(TAG, activity) {
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) {
                                try {
                                    try {
                                        Type modelType = new TypeToken<List<MainCategoryModel>>() {
                                        }.getType();
                                        List<MainCategoryModel> mArrayMainCategory = new Gson().fromJson(Objects.requireNonNull(jsonObject.optJSONArray("data")).toString(), modelType);

                                        contentApi(session, swipeRefresh, databaseMein, string, TAG, activity, new OnCompleteApi() {
                                            @Override
                                            public void onComplete() {
                                                new InsertCategory(activity, mArrayMainCategory, databaseMein, listener).execute();
                                            }
                                        });

                                    } catch (JsonSyntaxException e) {
                                        e.printStackTrace();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            protected void onFailed(int statusCode, String errorMessage) {
                            }

                            @Override
                            protected void onFailure(String failureMessage) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void contentApi(SessionManager session, SwipeRefreshLayout swipeRefresh, DatabaseMein databaseMein,
                                   String string, String TAG, AppCompatActivity activity, OnCompleteApi listener) {
        try {
            HashMap<String, String> params = new HashMap<>();
            if (session.isLoggedIn()) {
                params.put("id", "" + session.getUserDetail().getId());
            }
            try {
                Retrofit.with(activity)
                        .setAPI(APIs.CONTENT)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(TAG, activity) {
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) {
                                try {
                                    try {
                                        Type modelType = new TypeToken<List<SubContentModel>>() {
                                        }.getType();
                                        List<SubContentModel> mArrayContent = new Gson().fromJson(Objects.requireNonNull(jsonObject.optJSONArray("data")).toString(), modelType);

                                        new InsertContent(activity, mArrayContent, databaseMein, listener).execute();

                                       /* if (string.equalsIgnoreCase("")) {
                                            Intent intent = new Intent(activity, DashboardActivity.class);
                                            activity.startActivity(intent);
                                            activity.finish();
                                        }*/
                                        if (dialog != null && dialog.isShowing())
                                            dialog.dismiss();
                                        Log.e(TAG, "swipeRefresh........." + swipeRefresh);
                                        if (swipeRefresh != null) {
                                            swipeRefresh.setRefreshing(false);
                                        }
                                    } catch (JsonSyntaxException e) {
                                        e.printStackTrace();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            protected void onFailed(int statusCode, String errorMessage) {
                            }

                            @Override
                            protected void onFailure(String failureMessage) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void starTransactionApi(SessionManager session, SwipeRefreshLayout swipeRefresh, DatabaseMein databaseMein, String TAG, AppCompatActivity activity) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("qrcode", "6036003021000152");
            params.put("plz", "" + session.getUserDetail().getId());
            params.put("geb", "1990-01-01");
            try {
                Retrofit.with(activity)
                        .setAPI(APIs.STAR_TRANSAKTIONEN)
                        .setParameters(params)
                        .setCallBackListener(new JSONCallback(TAG, activity) {
                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) {
                                try {
                                    Type modelType = new TypeToken<List<StarTransaction>>() {
                                    }.getType();
                                    List<StarTransaction> mArrayMainCategory = new Gson().fromJson(jsonObject.optJSONObject("data").optJSONArray("Transaktionen").toString(), modelType);

                                    new InsertStarTransaction(activity, mArrayMainCategory, databaseMein).execute();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            protected void onFailed(int statusCode, String errorMessage) {
                            }

                            @Override
                            protected void onFailure(String failureMessage) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static void starTransactionApi(GustrowLoginModel gustrowLoginModel, SwipeRefreshLayout swipeRefresh,
                                          DatabaseMein databaseMein, String TAG, AppCompatActivity activity, OnCompleteApi listener) {
        try {
            Call<ResponseBody> call = ApiRetrofit.getClient().
                    starTransaction(gustrowLoginModel.getQRCode(), "" + gustrowLoginModel.getPostalCode(), gustrowLoginModel.getBirthDate());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            arrayStarTransactions.clear();
                            assert response.body() != null;
                            String responseString = response.body().string();
                            Log.e(TAG, "STAR--------------" + responseString);
                            JSONObject jsonObject = new JSONObject(responseString);

                            Type modelType = new TypeToken<List<StarTransaction>>() {
                            }.getType();
                            arrayStarTransactions = new Gson().fromJson(jsonObject.optJSONObject("data").optJSONArray("Transaktionen").toString(), modelType);

                            starHeaderApi(arrayStarTransactions, gustrowLoginModel, swipeRefresh, databaseMein, TAG, activity, listener);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    Log.e(TAG, "call...." + call.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void starHeaderApi(List<StarTransaction> arrayStarTransactions, GustrowLoginModel gustrowLoginModel, SwipeRefreshLayout swipeRefresh,
                                     DatabaseMein databaseMein, String TAG, AppCompatActivity activity, OnCompleteApi listener) {
        try {
            Call<ResponseBody> call = ApiRetrofit.getClient().
                    starHeader(gustrowLoginModel.getQRCode(), "" + gustrowLoginModel.getPostalCode(), gustrowLoginModel.getBirthDate());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            assert response.body() != null;
                            String responseString = response.body().string();
                            Log.e(TAG, "StarHeaderApi--------------" + responseString);
                            JSONObject jsonObject = new JSONObject(responseString);

                            JSONObject jsonObjectData = jsonObject.optJSONObject("data");
                            String punkte = jsonObjectData.optString("Punkte");
                            String guthaben = jsonObjectData.optString("Guthaben");
                            String gutscheinguthaben = jsonObjectData.optString("Gutscheinguthaben");

                           /* Type modelType = new TypeToken<StarHeaderModel>() {
                            }.getType();
                            StarHeaderModel starHeaderModel = new Gson().fromJson(jsonObject.optJSONObject("data").toString(), modelType);
*/
                            for (int i = 0; i < arrayStarTransactions.size(); i++) {
                                StarTransaction item = arrayStarTransactions.get(i);
                                item.setPunkte(punkte);
                                item.setGuthaben(guthaben);
                                item.setGutscheinguthaben(gutscheinguthaben);
                                arrayStarTransactions.set(i, item);
                            }
//                            new InsertStarHeader(activity, starHeaderModel, databaseMein).execute();
                            new InsertStarTransaction(activity, arrayStarTransactions, databaseMein, listener).execute();

                            if (swipeRefresh != null) {
                                swipeRefresh.setRefreshing(false);
                            }
                           /* StarTransaction starTransaction = new StarTransaction();
                            starTransaction.setPunkte(punkte);
                            arrayStarTransactions.add(starTransaction);*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    Log.e(TAG, "call...." + call.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showCustomConfirmationDialog(final Activity context, boolean showBottom, String titleText, String subTitleText, boolean showOneButton, String negativeText, String positiveText, final OnConfirmationDialog onConfirmationDialog) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context);
                final DialogAlertCustomBinding binding = DataBindingUtil.inflate(context.getLayoutInflater(), R.layout.dialog_alert_custom, null, false);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(ContextCompat.getDrawable(context, android.R.color.transparent));
                dialog.setContentView(binding.getRoot());
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                Window window = dialog.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                window.getAttributes().windowAnimations = R.style.animationdialog;

                binding.tvTitle.setText(titleText);

                if (!subTitleText.equals("")) {
                    binding.tvSubTitle.setText(subTitleText);
                    binding.tvSubTitle.setVisibility(View.VISIBLE);
                } else {
                    binding.tvSubTitle.setVisibility(View.GONE);
                }

                if (!negativeText.equals("")) {
                    binding.tvNegative.setText(negativeText);
                }

                if (!positiveText.equals("")) {
                    binding.tvPositive.setText(positiveText);
                }

                if (showBottom) {
                    binding.llBottom.setVisibility(View.VISIBLE);
                    binding.view.setVisibility(View.VISIBLE);
                } else {
                    binding.llBottom.setVisibility(View.GONE);
                    binding.view.setVisibility(View.INVISIBLE);
                }

                if (showOneButton) {
                    binding.tvNegative.setVisibility(View.GONE);
                    binding.viewDialog.setVisibility(View.GONE);
                } else {
                    binding.tvNegative.setVisibility(View.VISIBLE);
                    binding.viewDialog.setVisibility(View.VISIBLE);
                }

                binding.tvPositive.setOnClickListener(v -> {
                    dialog.dismiss();
                    dialog = null;
                    if (onConfirmationDialog != null) {
                        onConfirmationDialog.onYes();
                    }
                });

                binding.tvNegative.setOnClickListener(v -> {
                    dialog.dismiss();
                    if (onConfirmationDialog != null) {
                        onConfirmationDialog.onNo();
                    }
                });

                dialog.show();
                WindowManager.LayoutParams windowManager = context.getWindow().getAttributes();
                windowManager.dimAmount = 0.2f;
                context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
