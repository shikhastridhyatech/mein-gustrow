package com.meingustrow.app.util;

import android.text.TextUtils;
import android.widget.EditText;

import java.util.regex.Pattern;

public class ValidationUtils {

    /**
     * Check EditText is Empty or not
     * Input:
     * 1. EditText
     * Output: True / False
     */
    public static boolean isFieldEmpty(EditText et) {
        return TextUtils.isEmpty(et.getText().toString().trim());
    }

    /**
     * Regex pattern for Email Validation
     */
    private static String regexEmail =
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
    private final static Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(regexEmail, Pattern.CASE_INSENSITIVE);

    /**
     * Check Email String is Valid or Not
     * Input:
     * 1. Email String
     * Output: True / False
     */
    public static boolean isValidEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static boolean isValidPhoneNumber(CharSequence target) {
        return target.length() >= 8 && target.length() <= 15 && android.util.Patterns.PHONE.matcher(target).matches();
    }
}
