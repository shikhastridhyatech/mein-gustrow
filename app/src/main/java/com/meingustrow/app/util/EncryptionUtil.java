package com.meingustrow.app.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtil {

    private final String TRANSFORMATION = "AES/CBC/PKCS7PADDING";
    private final String SECRET_KEY_HASH_TRANSFORMATION;
    private final String CHARSET = "UTF-8";

    private static EncryptionUtil instance = null;
    private final Cipher writer;
    private final Cipher reader;

    public static class OnkoutSecurityException extends RuntimeException {

        OnkoutSecurityException(Throwable e) {
            super(e);
        }
    }

    public static EncryptionUtil getInstance() {
        if (instance == null) {
            instance = new EncryptionUtil();
        }
        return instance;
    }

    private EncryptionUtil() throws OnkoutSecurityException {
        try {
            this.writer = Cipher.getInstance(TRANSFORMATION);
            this.reader = Cipher.getInstance(TRANSFORMATION);

            initCiphers("(!Jack%Sparrow_Black{Pearl})~20@3");

        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            throw new OnkoutSecurityException(e);
        }
        this.SECRET_KEY_HASH_TRANSFORMATION = "SHA-256";
    }

    private void initCiphers(String secureKey)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException, InvalidAlgorithmParameterException {
        IvParameterSpec ivSpec = getIv();
        SecretKeySpec secretKey = getSecretKey(secureKey);

        Logger.e("SecureKey: ", secureKey);
        Logger.e("SecretKey: ", secretKey.getFormat().concat(" - ").concat(secretKey.getAlgorithm()).concat(" - ").concat(String.valueOf(secretKey.hashCode())).concat(" - ").concat(secretKey.toString()));

        writer.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
        reader.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
    }

    private IvParameterSpec getIv() {
        byte[] iv = new byte[writer.getBlockSize()];
        System.arraycopy("ABCDEFGHIJKLMNOP".getBytes(), 0,
                iv, 0, writer.getBlockSize());
        return new IvParameterSpec(iv);
    }

    private SecretKeySpec getSecretKey(String key)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] keyBytes = createKeyBytes(key);
        return new SecretKeySpec(keyBytes, TRANSFORMATION);
    }

    private byte[] createKeyBytes(String key)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.reset();
        return md.digest(key.getBytes(CHARSET));
    }

    private String encrypt(String value, Cipher writer)
            throws OnkoutSecurityException {
        byte[] secureValue;
        try {
            secureValue = convert(writer, value.getBytes(CHARSET));
        } catch (UnsupportedEncodingException e) {
            throw new OnkoutSecurityException(e);
        }
        return Base64.encodeToString(secureValue, Base64.NO_WRAP);
    }

    private String decrypt(String securedEncodedValue) {
        byte[] securedValue = Base64
                .decode(securedEncodedValue, Base64.NO_WRAP);
        byte[] value = convert(reader, securedValue);
        try {
            return new String(value, CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new OnkoutSecurityException(e);
        }
    }

    private static byte[] convert(Cipher cipher, byte[] bs)
            throws OnkoutSecurityException {
        try {
            return cipher.doFinal(bs);
        } catch (Exception e) {
            throw new OnkoutSecurityException(e);
        }
    }

    public String getEncodedValue(String data) {
        String secureValueEncoded = encrypt(data, writer);
        Logger.e("Encrypted String: ", secureValueEncoded);
        return secureValueEncoded;
    }

    public String getDecodedValue(String data) throws OnkoutSecurityException {
        String originalText = decrypt(data);
        Logger.e("Decrypted String: ", originalText);
        return originalText;
    }
}
