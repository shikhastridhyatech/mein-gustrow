package com.meingustrow.app.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.meingustrow.app.R;
import com.meingustrow.app.interfaces.OnConfirmationDialog;
import com.meingustrow.app.model.GustrowLoginModel;
import com.meingustrow.app.model.UserDataModel;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.fragment.HomeFragment;
import com.meingustrow.app.ui.fragment.LoginFragment;
import com.meingustrow.app.ui.fragment.LoginGustroCardFragment;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static org.apache.commons.codec.binary.Hex.encodeHex;

public class Utils {

    public static final String IS_LOGIN = "is_login";
    public static final String KEY_USER_MODEL = "user_model";
    private static Dialog dialog;

    public void changeFragment(AppCompatActivity context, Fragment fragment, String tagFragmentName) {

        FragmentManager mFragmentManager = context.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frameBottomMenu, fragmentTemp, tagFragmentName);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    public static Bitmap convertQRCode(String str) throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        Bitmap bitmap = null;
        try {
            BitMatrix bitMatrix = writer.encode(str, BarcodeFormat.QR_CODE, 300, 300);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            bitmap = bmp;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static void noInternet(Activity context) {
        AlertUtils.showCustomConfirmationDialog(context, context.getString(R.string.app_name), context.getString(R.string.no_internet_connection), true, "", context.getString(R.string.ok), new OnConfirmationDialog() {
            @Override
            public void onYes() {
            }

            @Override
            public void onNo() {
            }
        });
    }

    public static void dialodWithSingleButton(Activity context, String message) {
        AlertUtils.showCustomConfirmationDialog(context, context.getString(R.string.app_name), message, true, "", context.getString(R.string.ok), new OnConfirmationDialog() {
            @Override
            public void onYes() {
            }

            @Override
            public void onNo() {
            }
        });
    }

    public static boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }

    public static String convertMD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void replaceFragmentMainMenu(AppCompatActivity context, Fragment selectedFragment, boolean isBackStack) {
        try {
            String backStateName = selectedFragment.getClass().getName();
            FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frameMain, selectedFragment);
            if (isBackStack) {
                transaction.addToBackStack(null);
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void replaceFragmentDashboard(AppCompatActivity context, Fragment selectedFragment, boolean isBackStack, boolean isCheckCurrent) {
        try {
            String TAG = selectedFragment.getClass().getName();
          /*  int b = context.getSupportFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < b; i++) {
                context.getSupportFragmentManager().popBackStackImmediate();
            }*/
//            context.getSupportFragmentManager().popBackStack();
            if (selectedFragment instanceof HomeFragment) {
                context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                ((DashboardActivity) context).mBinding.includeHome.frameDashboardMain.setPadding(0, 0, 0, 0);
            } else {
                context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                ((DashboardActivity) context).mBinding.includeHome.frameDashboardMain.setPadding(0, ((DashboardActivity) context).height, 0, 0);
            }
            Fragment f = context.getSupportFragmentManager().findFragmentById(R.id.frameDashboardMain);
            if (isCheckCurrent && f != null && f.getClass().toString().equalsIgnoreCase(selectedFragment.getClass().toString())) {
                return;
            }
            Fragment fragmentTAG = context.getSupportFragmentManager().findFragmentByTag(TAG);
            Log.e(TAG, "fragmentTAG..............." + fragmentTAG);
            FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frameDashboardMain, selectedFragment, TAG);
            if (fragmentTAG == null) {
                if (isBackStack) {
                    transaction.addToBackStack(null);
                }
            } else if (context.getSupportFragmentManager().getBackStackEntryCount() == 1) {
                transaction.addToBackStack(null);
            } else if (selectedFragment instanceof LoginFragment && ((LoginFragment) selectedFragment).mString.equals(context.getString(R.string.from_settings))) {
                transaction.addToBackStack(null);
            } else if (selectedFragment instanceof LoginGustroCardFragment) {
                transaction.addToBackStack(null);
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void replaceFragmentDashboard(AppCompatActivity context, Fragment selectedFragment, boolean isBackStack) {
        try {
            String TAG = selectedFragment.getClass().getName();
          *//*  int b = context.getSupportFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < b; i++) {
                context.getSupportFragmentManager().popBackStackImmediate();
            }*//*
//            context.getSupportFragmentManager().popBackStack();
            Fragment fragmentTAG = context.getSupportFragmentManager().findFragmentByTag(TAG);
            Log.e(TAG, "fragmentTAG..............." + fragmentTAG);
            FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frameDashboardMain, selectedFragment, TAG);
            if (fragmentTAG == null) {
                if (isBackStack) {
                    transaction.addToBackStack(null);
                }
            } else if (context.getSupportFragmentManager().getBackStackEntryCount() == 1) {
                transaction.addToBackStack(null);
            } else if (selectedFragment instanceof LoginFragment && ((LoginFragment) selectedFragment).mString.equals(context.getString(R.string.from_settings))) {
                transaction.addToBackStack(null);
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static void replaceChildFragment(AppCompatActivity context, Fragment selectedFragment, boolean isBackStack) {
        try {
            String TAG = selectedFragment.getClass().getName();
          /*  int b = context.getSupportFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < b; i++) {
                context.getSupportFragmentManager().popBackStackImmediate();
            }*/
//            context.getSupportFragmentManager().popBackStack();
            new Handler().post(new Runnable() {
                public void run() {
                    Fragment fragmentTAG = context.getSupportFragmentManager().findFragmentByTag(TAG);
                    Log.e(TAG, "fragmentTAG..............." + fragmentTAG);
                    FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frameDashboardMain, selectedFragment, TAG);
                    if (fragmentTAG == null) {
                        if (isBackStack) {
                            transaction.addToBackStack(null);
                        }
                    } else if (context.getSupportFragmentManager().getBackStackEntryCount() == 1) {
                        transaction.addToBackStack(null);
                    }
                    transaction.commit();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addFragmentDashboard(AppCompatActivity context, Fragment selectedFragment, boolean isBackStack) {
        try {
            String TAG = selectedFragment.getClass().getName();
           /* int b = context.getSupportFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < b; i++) {
                context.getSupportFragmentManager().popBackStackImmediate();
            }*/
//            context.getSupportFragmentManager().popBackStack();
            Fragment fragmentTAG = context.getSupportFragmentManager().findFragmentByTag(TAG);
            Log.e(TAG, "fragmentTAG..............." + fragmentTAG);
            FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
            if (fragmentTAG == null) {
                if (isBackStack) {
                    transaction.addToBackStack(null);
                }
            }
            transaction.add(R.id.frameDashboardMain, selectedFragment, TAG);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setLocale(Activity activity, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getBaseContext().getResources().updateConfiguration(config, activity.getBaseContext().getResources().getDisplayMetrics());
    }

    public static String encode(String data) {
        Mac sha256_HMAC;
        String s = "";
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec("}{(*%$}&*^\\({#$%&@}^*$#{?}@^%#)(^%&".getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte digest[] = sha256_HMAC.doFinal(data.getBytes("UTF-8"));
            s = new String(encodeHex(digest));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static void showProgressBar(Context context) {
        dismissProgressBar();

        try {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_custom_dialog);

            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissProgressBar() {
        try {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    Logger.d("Network", "NETWORK NAME: " + networkInfo.getTypeName());
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Logger.d("Network", "NETWORK NAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    //Image Real Path from URI
    public static String getRealPathFromURI(Context mContext, Uri contentURI) {
        String result;
//        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        String[] filePathColumn = {MediaStore.Images.ImageColumns.DATA};
        Cursor cursor = mContext.getContentResolver().query(contentURI, filePathColumn, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(filePathColumn[0]);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static void setCurrentDate(AppCompatTextView tvDay, AppCompatTextView tvDate) {
        String[] strDate = TimeStamp.getCurrentDateElements();
        tvDay.setText(strDate[0]);
        tvDate.setText(strDate[1]);
    }

    public static void setVectorForPreLollipop(TextView textView, int resourceId, Context activity, int position) {
        Drawable icon;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            icon = VectorDrawableCompat.create(activity.getResources(), resourceId, activity.getTheme());
        } else {
            icon = activity.getResources().getDrawable(resourceId, activity.getTheme());
        }
        switch (position) {
            case AppConstants.DRAWABLE_LEFT:
                textView.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
                break;

            case AppConstants.DRAWABLE_RIGHT:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                break;

            case AppConstants.DRAWABLE_TOP:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null);
                break;

            case AppConstants.DRAWABLE_BOTTOM:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, icon);
                break;
        }
    }

    public static void setUserDetail(Context context, UserDataModel userModel) {
        Gson gson = new Gson();
        Type type = new TypeToken<UserDataModel>() {
        }.getType();
        String strObject = gson.toJson(userModel, type);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(KEY_USER_MODEL, strObject).apply();
    }

    public static UserDataModel getUserDetail(Context context) {
        Gson gson = new Gson();
        Type type = new TypeToken<UserDataModel>() {
        }.getType();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return gson.fromJson(prefs.getString(KEY_USER_MODEL, null), type);
    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(IS_LOGIN, false);
    }

    public static void setIsLoggedIn(Context context, boolean isLoggedIn) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putBoolean(IS_LOGIN, isLoggedIn).apply();
    }

    public static void storeDataByKey(Context context, String key, boolean Value) {
        getEditor(context).putBoolean(key, Value).apply();
    }

    public String getDataByKey(Context context, String Key, String DefaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String returnValue;
        if (prefs.contains(Key)) {
            returnValue = prefs.getString(Key, DefaultValue);
        } else {
            returnValue = DefaultValue;
        }
        return returnValue;
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.edit();
    }

    public static void storeDataByKey(Context context, String key, String Value) {
        getEditor(context).putString(key, Value).apply();
    }

    public void storeDataByKey(Context context, String key, long Value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putLong(key, Value).apply();
    }

    public static void setTheme(Activity context, int themeMode) {
        SharedPreferences prefs = context.getSharedPreferences("theme", Context.MODE_PRIVATE);
        prefs.edit().putInt("themeMode", themeMode).apply();
    }

    public static int getTheme(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("theme", Context.MODE_PRIVATE);
        return prefs.getInt("themeMode", -1);
    }

    public static void setAlarmGustrow(Context context, boolean isAlarmFinish) {
        SharedPreferences prefs = context.getSharedPreferences("alarmGustrow", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("timeGustrow", isAlarmFinish).apply();
    }

    public static boolean getAlarmGustrow(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("alarmGustrow", Context.MODE_PRIVATE);
        return prefs.getBoolean("timeGustrow", false);
    }

    public static void setAlarmLogin(Context context, boolean isAlarmFinish) {
        SharedPreferences prefs = context.getSharedPreferences("alarmLogin", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("timeLogin", isAlarmFinish).apply();
    }

    public static boolean getAlarmLogin(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("alarmLogin", Context.MODE_PRIVATE);
        return prefs.getBoolean("timeLogin", false);
    }

    public static void setLogInGustrow(Context context, boolean loggedIn) {
        SharedPreferences prefs = context.getSharedPreferences("loginGustrow", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("LoggedIn", loggedIn).apply();
    }

    public static boolean getLogInGustrow(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("loginGustrow", Context.MODE_PRIVATE);
        return prefs.getBoolean("LoggedIn", false);
    }

    public static void setGustrowData(Context context, GustrowLoginModel gustrowLoginModel) {
        Gson gson = new Gson();
        Type type = new TypeToken<GustrowLoginModel>() {
        }.getType();
        String strObject = gson.toJson(gustrowLoginModel, type);

        SharedPreferences prefs = context.getSharedPreferences("setGustrow", Context.MODE_PRIVATE);
        prefs.edit().putString("setData", strObject).apply();
    }

    public static GustrowLoginModel getGustrowData(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("setGustrow", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        Type type = new TypeToken<GustrowLoginModel>() {
        }.getType();

        return gson.fromJson(prefs.getString("setData", ""), type);
    }

    public static void clearGustrowData(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("setGustrow", Context.MODE_PRIVATE);
        prefs.edit().clear().apply();
    }

    public static void hideSoftKeyboard(Activity me, EditText et) {
        try {
            InputMethodManager imm = (InputMethodManager) me.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}