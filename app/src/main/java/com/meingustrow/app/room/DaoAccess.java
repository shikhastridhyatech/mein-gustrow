package com.meingustrow.app.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.StarHeaderModel;
import com.meingustrow.app.model.StarTransaction;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.model.WeatherModel;

import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface DaoAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(List<MainCategoryModel> categoryList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStarTransaction(List<StarTransaction> starTransactions);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStarHeader(StarHeaderModel starHeaderModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertWeather(WeatherModel weatherModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertContent(List<SubContentModel> subContentList);

    @Query("SELECT * FROM MainCategory")
    List<MainCategoryModel> getAllCategory();

/*    @Query("SELECT * FROM StarTransaction")
    List<StarTransaction> getAllStarTransaction();*/

    @Query("SELECT * FROM StarTransaction ORDER BY datetime(Zeitpunkt) desc")//
    List<StarTransaction> getAllStarTransaction();

    @Query("SELECT * FROM StarHeaderModel")
    StarHeaderModel getAllStarHeaders();

    @Query("SELECT * FROM SubContent")
    List<SubContentModel> getAllContent();

    @Query("SELECT * FROM Weather")
    WeatherModel getWeather();

    @Query("DELETE FROM StarTransaction")
    void deleteTransaction();

    @Query("DELETE FROM StarHeaderModel")
    void deleteHeader();

    @Query("DELETE FROM Weather")
    void deleteWeather();

//    @Query("SELECT * FROM SubContent")
//    List<SubContentModel> getAllContent();

   /* @Query("SELECT * FROM Note ORDER BY created_at desc")
    LiveData<List<ContactsContract.CommonDataKinds.Note>> fetchAllTasks();


    @Query("SELECT * FROM RoomParkingTable WHERE id =:taskId")
    LiveData<ContactsContract.CommonDataKinds.Note> getTask(int taskId);*/


  /*  @Update
    void updateTask(ContactsContract.CommonDataKinds.Note note);


    @Delete
    void deleteTask(ContactsContract.CommonDataKinds.Note note);

      @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(RoomParkingTable... roomParkingTables);


     @Query("SELECT * FROM RoomParkingTable")
    List<RoomParkingTable> getAllParking();

    @Query("SELECT * FROM RoomParkingTable where mallId=:mallId ORDER BY id DESC")
    List<RoomParkingTable> getParkingByMall(int mallId);

    @Query("DELETE FROM RoomParkingTable where id = :parkingId")
    void deleteSelected(int parkingId);
*/

}