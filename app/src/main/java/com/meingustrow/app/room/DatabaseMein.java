package com.meingustrow.app.room;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.StarHeaderModel;
import com.meingustrow.app.model.StarTransaction;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.model.TypeConvertersCategory;
import com.meingustrow.app.model.TypeConvertersContent;
import com.meingustrow.app.model.WeatherModel;

@androidx.room.Database(entities = {MainCategoryModel.class,
        SubContentModel.class, WeatherModel.class, StarTransaction.class, StarHeaderModel.class}, version = 2, exportSchema = false)
@TypeConverters({TypeConvertersCategory.class, TypeConvertersContent.class})
public abstract class DatabaseMein extends RoomDatabase {

    private static final String DB_NAME = "MeinGustrow.db";

    public abstract DaoAccess daoAccess();

    private static volatile DatabaseMein instance;

    public static synchronized DatabaseMein getInstance(Context context) {

        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static DatabaseMein create(Context context) {

        return Room.databaseBuilder(
                context,
                DatabaseMein.class,
                DB_NAME)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }
  /*  static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE StarTransaction "
                    + " ADD COLUMN 'Guthaben' STRING, Guthaben STRING, ");
        }
    };*/
}
