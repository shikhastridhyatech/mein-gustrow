package com.meingustrow.app.webservice;

import static com.meingustrow.app.util.AppConstants.BASE_DOMAIN;

public class APIs {

    public static final String BASE_URL = BASE_DOMAIN + "api/";
    public static final String BASE_IMAGE_PATH = BASE_DOMAIN;
    public static final String BASE_VIDEO_PATH = BASE_DOMAIN;
    public static final String LOCATION_URL = "https://maps.googleapis.com/maps/api/geocode/";

    public static final String LOAD_MORE_LIMIT = "10";

    public static final String CONTENT = "content/";
    public static final String CATEGORY = "category/";
    public static final String WEATHER = "weather/";
    public static final String LOGIN = "login/";
    public static final String STAR_TRANSAKTIONEN = "guestrow/transaktionen/";
    public static final String STAR_HEADER = "guestrow/guthaben/";
    public static final String TEMP_WEB = "https://www.stadtwerke-guestrow.de/unser-service/kontakt/";
    public static final String PARTNER = "http://www.guestrowcard.de/partner";

    //Authentication
    public static final String API_LOGIN = "auth/login";

    public static final String API_LOGOUT = "auth/logout";
    public static final String API_FORGOT_PASSWORD = "auth/forgot/password";
    public static final String API_REQUEST_RESET_PASSWORD = "auth/request/reset-password";
    public static final String API_REFRESH_TOKEN = "auth/update/device-token";
    public static final String API_VERIFY_OTP = "auth/login/verify/otp";

}