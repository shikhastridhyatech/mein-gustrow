package com.meingustrow.app.webservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ApiInterface {

    @POST
    Call<ResponseBody> callPostMethod(@Url String url, @Body HashMap<String, String> body);

    @Multipart
    @POST
    Call<ResponseBody> callPostMethodMultipart(@Url String url, @PartMap() Map<String, String> partMap, @Part List<MultipartBody.Part> files);

    @POST
    Call<ResponseBody> callPostMethod(@Url String url, @Body RequestBody body);

    @GET
    Call<ResponseBody> callGetMethod(@Url String url);
}
