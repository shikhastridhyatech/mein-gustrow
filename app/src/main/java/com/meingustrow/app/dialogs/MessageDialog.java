package com.meingustrow.app.dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.DialogMessageBinding;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public class MessageDialog extends AlertDialog implements View.OnClickListener {
    private boolean cancelable = true, onlyPositiveButton = false;
    private String title, message, positiveButtonText, negativeButtonText;
    private OnClickListener onPositiveButtonClick, onNegativeButtonClick;
    private boolean isChecked, isAutoDismiss;
    private Drawable drawable;

    public MessageDialog(Context context) {
        super(context);
    }

    public MessageDialog(Context context, boolean onlyPositiveButton) {
        super(context);
        this.onlyPositiveButton = onlyPositiveButton;
    }

    public MessageDialog setTitle(String title) {
        this.title = title;
        return this;
    }

    public MessageDialog setEvent(boolean isChecked, Drawable drawable) {
        this.isChecked = isChecked;
        this.drawable = drawable;
        return this;
    }

    public MessageDialog setMessage(String message) {
        this.message = message;
        return this;
    }

    public MessageDialog isAutoDismiss(boolean isAutoDismiss) {
        this.isAutoDismiss = isAutoDismiss;
        return this;
    }

    public MessageDialog cancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public MessageDialog setPositiveButton(String text, OnClickListener listener) {
        this.positiveButtonText = text;
        this.onPositiveButtonClick = listener;
        return this;
    }

    public MessageDialog setNegativeButton(String text, OnClickListener listener) {
        this.negativeButtonText = text;
        this.onNegativeButtonClick = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogMessageBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_message, null, false);
        setContentView(mBinder.getRoot());
        setCanceledOnTouchOutside(cancelable);
        setCancelable(cancelable);
        mBinder.tvTitle.setVisibility(title != null ? View.VISIBLE : View.GONE);
        if (title != null) mBinder.tvTitle.setText(title);

        mBinder.tvMessage.setVisibility(message != null ? View.VISIBLE : View.GONE);
        if (message != null) mBinder.tvMessage.setText(message);

        mBinder.ivChecked.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        if (drawable != null) mBinder.ivChecked.setImageDrawable(drawable);

        if (positiveButtonText != null) mBinder.tvButtonPositive.setText(positiveButtonText);
        if (negativeButtonText != null) mBinder.tvButtonNegative.setText(negativeButtonText);

        mBinder.tvButtonPositive.setVisibility(onPositiveButtonClick != null ? View.VISIBLE : View.GONE);
        mBinder.tvButtonNegative.setVisibility(!onlyPositiveButton && onNegativeButtonClick != null ? View.VISIBLE : View.GONE);

        mBinder.tvButtonPositive.setOnClickListener(this);
        mBinder.tvButtonNegative.setOnClickListener(this);
        if (isAutoDismiss) {
            final Timer timer2 = new Timer();
            timer2.schedule(new TimerTask() {
                public void run() {
                    dismiss();
                    timer2.cancel(); //this will cancel the timer of the system
                }
            }, 5000); // the timer will count 5 seconds....
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvButtonPositive:
                if (onPositiveButtonClick != null)
                    onPositiveButtonClick.onClick(MessageDialog.this, 0);
                break;
            case R.id.tvButtonNegative:
                if (onNegativeButtonClick != null)
                    onNegativeButtonClick.onClick(MessageDialog.this, 0);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
}