package com.meingustrow.app.interfaces;

public interface OnSnackBarActionListener {
    void onAction();
}
