package com.meingustrow.app.interfaces;

/**
 * Created by Disha on 26-06-2019.
 */

public interface OnCompleteApi {
    void onComplete();
}
