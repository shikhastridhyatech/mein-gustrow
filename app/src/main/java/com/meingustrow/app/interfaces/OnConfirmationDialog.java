package com.meingustrow.app.interfaces;

/**
 * Created by Disha on 26-06-2019.
 */

public interface OnConfirmationDialog {
    void onYes();

    void onNo();
}
