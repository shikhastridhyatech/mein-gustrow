package com.meingustrow.app.interfaces;

/**
 * Created by viraj.patel on 10-Sep-18
 */
public interface OnRecyclerViewItemClicked<T> {
    void onItemClicked(T object);
}
