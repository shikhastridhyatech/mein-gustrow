package com.meingustrow.app.Retrofit;

import com.meingustrow.app.webservice.APIs;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Disha on 05-08-2019.
 */

public interface RetrofitInterface {

    @FormUrlEncoded
    @POST(APIs.LOGIN)
    Call<ResponseBody> signInUser(@Field("email") String email,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST(APIs.STAR_HEADER)
    Call<ResponseBody> signInGustroUser(@Field("qrcode") String qrcode,
                                        @Field("plz") String plz,
                                        @Field("geb") String geb);

    @FormUrlEncoded
    @POST(APIs.STAR_TRANSAKTIONEN)
    Call<ResponseBody> starTransaction(@Field("qrcode") String qrcode,
                                       @Field("plz") String plz,
                                       @Field("geb") String geb);

    @FormUrlEncoded
    @POST(APIs.STAR_HEADER)
    Call<ResponseBody> starHeader(@Field("qrcode") String qrcode,
                                  @Field("plz") String plz,
                                  @Field("geb") String geb);

}
