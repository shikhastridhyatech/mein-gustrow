package com.meingustrow.app.Retrofit;

import com.meingustrow.app.webservice.APIs;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Disha on 05-08-2019.
 */

public class ApiRetrofit {

    public static RetrofitInterface getClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

       /* OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS).build();*/

//        client = httpClient.build();

        // change your base URL
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(APIs.BASE_URL) //Set the Root URL
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build(); //Finally building the adapter

        //Creating object for our interface
        RetrofitInterface api = adapter.create(RetrofitInterface.class);
        return api; // return the APIInterface object
    }
}
