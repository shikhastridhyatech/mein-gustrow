package com.meingustrow.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ItemMainMenuBinding;
import com.meingustrow.app.interfaces.OnRecyclerViewItemClicked;
import com.meingustrow.app.model.MenuTitleModel;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.util.Utils;

import java.util.List;

public class MainMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = MainMenuAdapter.class.getName();
    private Activity mContext;
    private int isSelected = -1;
    private List<SubContentModel> mArrayMain;
    private OnRecyclerViewItemClicked mInterface;

    public MainMenuAdapter(Activity context, List<SubContentModel> arrayMain, OnRecyclerViewItemClicked recyclerInterface) {
        this.mContext = context;
        this.mInterface = recyclerInterface;
        this.mArrayMain = arrayMain;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemMainMenuBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_main_menu, parent, false);
        return new AdapterItem(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            ((AdapterItem) holder).binding.tvMenu.setText(mArrayMain.get(position).getTitle());
//            ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(mArrayWhiteImages.get(position));

            if (position == 0) {
//                if (Utils.getTheme(mContext) == R.style.ThemeLight) {
                    ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.ic_start_page);
//                } else {
//                    ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.white_card_menu);
//                }
            } else if (position == 1) {
                ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.ic_card);

//                if (Utils.getTheme(mContext) == R.style.ThemeLight) {
//                } else {
//                    ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.white_card_menu);
//                }
            } else if (position == (mArrayMain.size() - 1)) {
                ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.ic_settings);

//                if (Utils.getTheme(mContext) == R.style.ThemeLight) {
//                } else {
//                    ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.white_settigns);
//                }
            } else {
                if (Utils.getTheme(mContext) == R.style.ThemeLight) {
                    Glide.with(mContext)
                            .load(mArrayMain.get(position).getIcon().getLight())
                            .into(((AdapterItem) holder).binding.imgMenuIcons);
                } else {
                    Glide.with(mContext)
                            .load(mArrayMain.get(position).getIcon().getDark())
                            .into(((AdapterItem) holder).binding.imgMenuIcons);
                }
            }

            ((AdapterItem) holder).binding.clMenuItem.setOnClickListener(view -> {
                MenuTitleModel menuTitleModel = new MenuTitleModel();
                menuTitleModel.setTitleName(mArrayMain.get(position).getTitle());
                menuTitleModel.setPosition(position);
                mInterface.onItemClicked(menuTitleModel);
            });

            // Red Image selection
           /* ((AdapterItem) holder).binding.tvMenu.setText(mArrayMenuNames.get(position));
            if (isSelected == position) {
                ((AdapterItem) holder).binding.tvMenu.setSelected(true);
                Glide.with(mContext)
                        .load(mArrayRedImages.get(position))
                        .into(((AdapterItem) holder).binding.imgMenuIcons);
                MenuTitleModel menuTitleModel = new MenuTitleModel();
                menuTitleModel.setTitleName(mArrayMenuNames.get(position));
                menuTitleModel.setPosition(position);
                mInterface.onItemClicked(menuTitleModel);
            } else {
                ((AdapterItem) holder).binding.tvMenu.setSelected(false);
//                ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(mArrayWhiteImages.get(position));

                if (position == 0) {
                    ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.blue_card_menu);
                } else if (position == (mArrayMenuNames.size() - 1)) {
                    ((AdapterItem) holder).binding.imgMenuIcons.setImageResource(R.drawable.blue_settings);
                } else {
                    Glide.with(mContext)
                            .load(mArrayWhiteImages.get(position))
                            .into(((AdapterItem) holder).binding.imgMenuIcons);
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSelection(String string) {
       /* isSelected = (mArrayMenuNames.size() - 1);
        notifyDataSetChanged();*/
    }

    @Override
    public int getItemCount() {
        return mArrayMain.size();
    }

    class AdapterItem extends RecyclerView.ViewHolder {
        ItemMainMenuBinding binding;

        AdapterItem(@NonNull ItemMainMenuBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;

           /* binding.clMenuItem.setOnClickListener(view -> {
                isSelected = getAdapterPosition();
                notifyDataSetChanged();
            });*/
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
