package com.meingustrow.app.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ItemHomeSubBinding;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.fragment.BottomMenuFragment;
import com.meingustrow.app.ui.fragment.LoginGustroCardFragment;
import com.meingustrow.app.ui.fragment.SubMenuFragment;
import com.meingustrow.app.ui.fragment.WebFragment;
import com.meingustrow.app.util.Utils;

import java.util.List;
import java.util.Objects;

public class HomeSubAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = HomeSubAdapter.class.getName();
    private Context mContext;
    private List<SubContentModel> mArrayHome;
    private int mainPosition;

    public HomeSubAdapter(Context context, List<SubContentModel> arrayHome, int position) {
        this.mContext = context;
        this.mArrayHome = arrayHome;
        this.mainPosition = position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemHomeSubBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_home_sub, parent, false);
        return new AdapterItem(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            ((AdapterItem) holder).binding.tvTitle.setText(mArrayHome.get(position).getTitle());
            Glide.with(mContext)
                    .asBitmap()
                    .load(mArrayHome.get(position).getImage())
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            ((AdapterItem) holder).binding.imgHome.setImageBitmap(resource);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });

            ((AdapterItem) holder).binding.imgHome.setOnClickListener(view -> {

                if (mainPosition == 0 && position == 0) {
                    try {
                        ((DashboardActivity) Objects.requireNonNull(mContext)).mBinding.includeHome.includeDashboard.clHomeToolbar.setVisibility(View.GONE);
                        ((DashboardActivity) Objects.requireNonNull(mContext)).mBinding.includeHome.includeDashboard.clMainToolbar.setVisibility(View.VISIBLE);

                        if (Utils.getLogInGustrow(mContext)) {
                            Utils.replaceFragmentDashboard((AppCompatActivity) mContext, new BottomMenuFragment(), true, true);
                        } else {
                            Utils.replaceFragmentDashboard((AppCompatActivity) mContext, new LoginGustroCardFragment(""), true, true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (mArrayHome.get(position).getSubContent() == null) {
                    if (mArrayHome.get(position).getUrl() != null) {
                        Utils.replaceFragmentDashboard((DashboardActivity) mContext, new WebFragment(mArrayHome.get(position).getUrl(), mArrayHome.get(position).getTitle()), true, true);
                    }
                } else {
                    Utils.replaceFragmentDashboard((DashboardActivity) mContext, new SubMenuFragment(mArrayHome.get(position).getSubContent(), mArrayHome.get(position).getLogo(), mArrayHome.get(position).getTitle()), true, true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrayHome.size();
    }

    class AdapterItem extends RecyclerView.ViewHolder {
        ItemHomeSubBinding binding;

        AdapterItem(@NonNull ItemHomeSubBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
