package com.meingustrow.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ItemStarBinding;
import com.meingustrow.app.model.StarTransaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class StarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = StarAdapter.class.getName();
    private Context mContext;
    private List<StarTransaction> mArrayTransaction;

    public StarAdapter(Context context, List<StarTransaction> arrayTransaction) {
        this.mContext = context;
        this.mArrayTransaction = arrayTransaction;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemStarBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_star, parent, false);
        return new AdapterItem(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            String[] split = mArrayTransaction.get(position).getZeitpunkt().split(" ");
            ((AdapterItem) holder).binding.tvStarCount.setText("" + mArrayTransaction.get(position).getBonusPunkte()+" "+ mContext.getString(R.string.punkte_cap));
            ((AdapterItem) holder).binding.tvStarDate.setText(changeDate(mArrayTransaction.get(position).getZeitpunkt()));
            ((AdapterItem) holder).binding.tvStarDetails.setText(mArrayTransaction.get(position).getAkzeptanzstelle());
            ((AdapterItem) holder).binding.tvBonusValue.setText(mArrayTransaction.get(position).getBonusEuro() + mContext.getString(R.string.EURO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String changeDate(String date) throws ParseException {
//        2015-06-18 13:40:47
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = spf.parse(date);
        spf = new SimpleDateFormat("dd.MM.yyyy");
        date = spf.format(newDate);
        return date;
    }

    public void setArray(List<StarTransaction> arrayTransaction) {
        this.mArrayTransaction = arrayTransaction;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mArrayTransaction.size();
    }

    class AdapterItem extends RecyclerView.ViewHolder {
        ItemStarBinding binding;

        AdapterItem(@NonNull ItemStarBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;

            binding.clSubMenuItem.setOnClickListener(view -> {

            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
