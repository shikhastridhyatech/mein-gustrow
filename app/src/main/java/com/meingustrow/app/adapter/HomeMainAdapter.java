package com.meingustrow.app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ItemHomeMainBinding;
import com.meingustrow.app.databinding.ItemStarBinding;
import com.meingustrow.app.model.HomeModel;
import com.meingustrow.app.model.Icon;
import com.meingustrow.app.model.Logo;
import com.meingustrow.app.model.MainCategoryModel;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.room.DatabaseMein;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = HomeMainAdapter.class.getName();
    private Context mContext;
    private List<MainCategoryModel> mArrayMainCategory;

    public HomeMainAdapter(Context context, List<MainCategoryModel> arrayMainCategory, DatabaseMein databaseMein) {
        this.mContext = context;
        this.mArrayMainCategory = arrayMainCategory;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemHomeMainBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_home_main, parent, false);
        return new AdapterItem(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            ((AdapterItem) holder).binding.tvHomeTitle.setText(mArrayMainCategory.get(position).getTitle());

            setAdapter(position, ((AdapterItem) holder).binding, mArrayMainCategory.get(position).getArraySubContnet());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrayMainCategory.size();
    }

    class AdapterItem extends RecyclerView.ViewHolder {
        ItemHomeMainBinding binding;

        AdapterItem(@NonNull ItemHomeMainBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }
    }

    public void setArray(List<MainCategoryModel> arrayMainCategory) {
        this.mArrayMainCategory = arrayMainCategory;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void setAdapter(int position, ItemHomeMainBinding binding, List<SubContentModel> arraySubContent) {
        try {
            if (position == 0) {
                SubContentModel subContentModel = new SubContentModel();
                subContentModel.setTitle(mContext.getString(R.string.gustrow_card));
                subContentModel.setImage("https://www.mgapp.psnmedia.cloud/api/images/bildkachel_01.jpg");
                arraySubContent.add(0, subContentModel);
            }

            HomeSubAdapter mHomeSubAdapter = new HomeSubAdapter(mContext, arraySubContent, position);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
            binding.rvHomeSub.setLayoutManager(layoutManager);
            binding.rvHomeSub.setAdapter(mHomeSubAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
