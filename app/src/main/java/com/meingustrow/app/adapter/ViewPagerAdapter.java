package com.meingustrow.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ItemViewPagerBinding;

import java.util.ArrayList;

/**
 * Created by Disha on 14-11-2019.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private static final String TAG = ViewPagerAdapter.class.getName();
    private Context mContext;
    private boolean mClick = false;
    private ArrayList<Integer> mArraySlider;

    public ViewPagerAdapter(Context context, ArrayList<Integer> arraySlider, boolean click) {
        this.mContext = context;
        this.mClick = click;
        this.mArraySlider = arraySlider;
    }

    @Override
    public int getCount() {
        return mArraySlider.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((ConstraintLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_view_pager, container, false);

        ImageView imageView = view.findViewById(R.id.imgSlider);
        Glide.with(mContext)
                .load(mArraySlider.get(position))
                .into(imageView);
        container.addView(view);
        return view;
    }

    /*  LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      ItemViewPagerBinding binding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.item_view_pager, container, false);

          Glide.with(mContext)
              .load(mArraySlider.get(position))
              .into(binding.imgSlider);

          return binding.getRoot();
  */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        try {
            ((ViewPager) container).removeView((ConstraintLayout) object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
