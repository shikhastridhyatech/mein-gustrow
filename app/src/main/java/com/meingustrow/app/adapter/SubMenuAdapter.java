package com.meingustrow.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.meingustrow.app.R;
import com.meingustrow.app.databinding.ItemSubMenuBinding;
import com.meingustrow.app.model.SubContentModel;
import com.meingustrow.app.ui.activity.DashboardActivity;
import com.meingustrow.app.ui.fragment.WebFragment;
import com.meingustrow.app.util.Utils;

import java.util.List;

public class SubMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SubMenuAdapter.class.getName();
    private Context mContext;
    private List<SubContentModel> mArrayMain;
    private String mTitle;

    public SubMenuAdapter(Context context, List<SubContentModel> list, String title) {
        this.mContext = context;
        this.mArrayMain = list;
        this.mTitle = title;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemSubMenuBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_sub_menu, parent, false);
        return new AdapterItem(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            ((AdapterItem) holder).binding.tvSubMenu.setText(mArrayMain.get(position).getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrayMain.size();
    }

    class AdapterItem extends RecyclerView.ViewHolder {
        ItemSubMenuBinding binding;

        AdapterItem(@NonNull ItemSubMenuBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;

            binding.clSubMenuItem.setOnClickListener(view -> {
                if (mArrayMain.get(getAdapterPosition()).getSubContent() == null) {
                    if (mArrayMain.get(getAdapterPosition()).getUrl() != null) {
                        Utils.replaceFragmentDashboard((DashboardActivity) mContext, new WebFragment(mTitle, mArrayMain.get(getAdapterPosition()).getUrl(), mArrayMain.get(getAdapterPosition()).getTitle()), true, true);
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
