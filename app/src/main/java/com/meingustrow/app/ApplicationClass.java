package com.meingustrow.app;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;

//import io.fabric.sdk.android.Fabric;

//   chrome://inspect/#devices

public class ApplicationClass extends Application {

    private static ApplicationClass context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

//        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(this);

        Stetho.initializeWithDefaults(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public static Context getAppContext() {
        return context;
    }
}