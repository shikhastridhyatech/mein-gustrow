package com.meingustrow.app.broadcast;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.meingustrow.app.ui.activity.DialogActivity;
import com.meingustrow.app.ui.fragment.LoginGustroCardFragment;
import com.meingustrow.app.util.AppConstants;
import com.meingustrow.app.util.Utils;

import java.util.Calendar;

public class MyAlarm extends BroadcastReceiver {
    private static final String TAG = MyAlarm.class.getName();

    // this constructor is called by the alarm manager.
    public MyAlarm() {
    }

    // you can use this constructor to create the alarm.
    //  Just pass in the main activity as the context,
    //  any extras you'd like to get later when triggered
    //  and the timeout

    public MyAlarm(Activity context, int timeoutInSeconds, String whichClass, int requestCode) {
        try {
            AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, MyAlarm.class);
            intent.putExtra(AppConstants.ALARM_MANAGER_CLASS, whichClass);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Calendar time = Calendar.getInstance();
            time.setTimeInMillis(System.currentTimeMillis());
            time.add(Calendar.SECOND, timeoutInSeconds);
            alarmMgr.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Log.e(TAG, "---------------onReceive-------------------");
            if (intent.getStringExtra(AppConstants.ALARM_MANAGER_CLASS).equals(LoginGustroCardFragment.class.getName())) {
                Utils.setAlarmGustrow(context, true);
            } else {
                Utils.setAlarmLogin(context, true);
            }
            if (Utils.isAppOnForeground(context, context.getPackageName()))
                context.startActivity(new Intent(context, DialogActivity.class)
                        .putExtra(AppConstants.ALARM_MANAGER_DIALOG, intent.getStringExtra(AppConstants.ALARM_MANAGER_CLASS))
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
